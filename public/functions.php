<?php
/**
 * @package Springbook - Responsive WordPress Blog Travel Photography Theme
 * @author CTHthemes - http://themeforest.net/user/cththemes
 * @date: 10-7-2016
 * @version: 1.0
 * @copyright  Copyright ( C ) 2015 cththemes.com . All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 */

if (!isset($springbook_options) && file_exists(get_template_directory() . '/functions/admin-config.php')) {
    require_once (get_template_directory() . '/functions/admin-config.php');
}
if(!function_exists('springbook_content_width')){
    /**
     * Sets the content width in pixels, based on the theme's design and stylesheet.
     *
     * Priority 0 to make it available to lower priority callbacks.
     *
     * @global int $content_width
     *
     * @since Springbook 1.0
     */
    function springbook_content_width() {
        $GLOBALS['content_width'] = apply_filters( 'springbook_content_width', 616 );
    }
    add_action( 'after_setup_theme', 'springbook_content_width', 0 );

}

if (!function_exists('springbook_setup_theme')) {
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     *
     * @since Springbook 1.0
     */

    function springbook_setup_theme() {
        global $springbook_options;
        $lang = get_template_directory() . '/languages';
        load_theme_textdomain('springbook', $lang);
        
        add_theme_support('post-thumbnails');

        if(isset($springbook_options['enable_custom_sizes']) && $springbook_options['enable_custom_sizes']){
            //get thumbnail sizes option
            $sizes = springbook_get_thumbnail_size();
            
            add_image_size('springbook-hero', $sizes['herothumb']['width'], $sizes['herothumb']['height'], $sizes['herothumb']['hard_crop']);

            add_image_size('springbook-thumb', $sizes['postthumb']['width'], $sizes['postthumb']['height'], $sizes['postthumb']['hard_crop']);
            add_image_size('springbook-large', $sizes['postsingthumb']['width'], $sizes['postsingthumb']['height'], $sizes['postsingthumb']['hard_crop']);
            add_image_size('springbook-related', $sizes['postrelated']['width'], $sizes['postrelated']['height'], $sizes['postrelated']['hard_crop']);
            add_image_size('springbook-gallery-grid', $sizes['galgrid']['width'], $sizes['galgrid']['height'], $sizes['galgrid']['hard_crop']);
            add_image_size('springbook-gallery', $sizes['galma']['width'], $sizes['galma']['height'], $sizes['galma']['hard_crop']);
            
        }
        
        
        // Adds RSS feed links to <head> for posts and comments.
        add_theme_support('automatic-feed-links');
        
        // Switches default core markup for search form, comment form, and comments
        
        add_theme_support('title-tag');
        
        // to output valid HTML5.
        add_theme_support('html5', array('search-form', 'comment-form', 'comment-list'));
        
        //Post formats
        add_theme_support('post-formats', array('audio', 'gallery', 'image', 'link', 'quote', 'status', 'video','slider'));

        add_theme_support('custom-header');

        add_theme_support('custom-background');

        // from version 2.0
        add_theme_support( 'woocommerce' );

        register_nav_menus(
            array(
                'primary' => esc_html__('Primary Menu', 'springbook'),
                'right-fixed-icons' => esc_html__('Right fixed icons', 'springbook'),
            )
        );
        
        add_filter('use_default_gallery_style', '__return_false');

        add_editor_style(get_template_directory_uri().'/inc/assets/custom-editor-style.css');
    }
}
add_action('after_setup_theme', 'springbook_setup_theme');

if (!function_exists('springbook_set_default_options')) {
    function springbook_set_default_options(){
        global $springbook_options;
        /* '.addcslashes( esc_url( home_url( '/' ) ) , '/').' */
        if(!isset($springbook_options) || empty($springbook_options)){
            $springbook_default_options = '{"favicon":{"url":"'.addcslashes( esc_url( home_url( '/' ) ) , '/').'wp-content\/themes\/springbook\/images\/favicon.ico"},"logo_first":{"url":"'.addcslashes( esc_url( home_url( '/' ) ) , '/').'wp-content\/themes\/springbook\/images\/logo.png"},"logo_size_width":"350","logo_size_height":"131","logo_text":"","slogan":"","to_top_icon":"<i class=\"fa fa-angle-double-up\"><\/i>","show_submenu_mobile":false,"video_header_mute":true,"show_blog_breadcrumbs":false,"show_categories_nav":false,"header_logo":{"url":"'.addcslashes( esc_url( home_url( '/' ) ) , '/').'wp-content\/themes\/springbook\/images\/logo_header.png"},"show_topbar":true,"topbar_content":"<div class=\"top-bar-social fl-left\">\n    <ul>\n        <li><a href=\"#\" target=\"_blank\" ><i class=\"fa fa-facebook\"><\/i><\/a><\/li>\n        <li><a href=\"#\" target=\"_blank\"><i class=\"fa fa-twitter\"><\/i><\/a><\/li>\n        <li><a href=\"#\" target=\"_blank\" ><i class=\"fa fa-instagram\"><\/i><\/a><\/li>\n        <li><a href=\"#\" target=\"_blank\" ><i class=\"fa fa-pinterest\"><\/i><\/a><\/li>\n        <li><a href=\"#\" target=\"_blank\" ><i class=\"fa fa-tumblr\"><\/i><\/a><\/li>\n    <\/ul>\n<\/div>\n<ul class=\"top-bar-contacts fl-right\">\n    <li><span>Phone:<\/span><a href=\"#\">+7(111)123456789<\/a><\/li>\n    <li><span>Mail : <\/span><a href=\"mailto:yourmail@domian.com?Subject=Contact%20Us\">yourmail@domian.com<\/a><\/li>\n<\/ul>","show_topsearch":true,"show_fixed_sidebar":false,"fixed_sidebar_width":"350","header_color":"dark","footer_style":"big","footer_quote_widget":"<div class=\"row\">\n    <div class=\"col-md-12\">\n        <div class=\"quote-widget\">\n            <h3>Quote widget<\/h3>\n            <p>\" Nulla fringilla purus at leo dignissim congue. Mauris elementum accumsan leo vel tempor. Sit amet cursus nisl aliquam. Aliquam et elit eu nunc. \"<\/p>\n        <\/div>\n    <\/div>\n<\/div>","footer_minimal_content":"<div class=\"full-width-instagram\">\n<h6><i class=\"fa fa-instagram\"><\/i>#springbook<\/h6>\n[instagram-feed num=8 cols=8 imagepadding=0]\n<\/div>\n<div class=\"clearfix\"><\/div>\n<div class=\"container\">\n    <div class=\"logo-inner\">\n        <div class=\"logo-holder\">\n            <a href=\"'.addcslashes( esc_url( home_url( '/' ) ) , '/').'\"><img src=\"'.addcslashes( esc_url( home_url( '/' ) ) , '/').'wp-content\/themes\/springbook\/images\/logo3.png\" ><\/a>\n        <\/div>\n        <p>\" A super cool template for bloggers, photographers and travelers \"<\/p>\n    <\/div>\n<\/div>","footer_content":"<div class=\"row\">\n    <div class=\"col-md-6\">\n        <div class=\"copyright\">\n            <span>&copy; Spring book 2016 .<\/span> All rights reserved \n        <\/div>\n    <\/div>\n    <div class=\"col-md-6\">\n        <ul class=\"sub-footer-social\">\n            <li><a href=\"#\" target=\"_blank\" ><i class=\"fa fa-facebook\"><\/i><\/a><\/li>\n            <li><a href=\"#\" target=\"_blank\"><i class=\"fa fa-twitter\"><\/i><\/a><\/li>\n            <li><a href=\"#\" target=\"_blank\" ><i class=\"fa fa-instagram\"><\/i><\/a><\/li>\n            <li><a href=\"#\" target=\"_blank\" ><i class=\"fa fa-pinterest\"><\/i><\/a><\/li>\n            <li><a href=\"#\" target=\"_blank\" ><i class=\"fa fa-tumblr\"><\/i><\/a><\/li>\n        <\/ul>\n    <\/div>\n<\/div>","footer_color":"dark","enable_custom_sizes":false,"hero_header_thumb":{"width":"9999","height":"9999","hard_crop":1},"blog_post_thumb":{"width":"9999","height":"9999","hard_crop":1},"blog_single_post_thumb":{"width":"9999","height":"9999","hard_crop":1},"related_post_thumb":{"width":"9999","height":"9999","hard_crop":1},"gallery_grid_thumb":{"width":"9999","height":"9999","hard_crop":1},"gallery_masonry_thumb":{"width":"9999","height":"9999","hard_crop":1},"mailchimp_api":"","mailchimp_list_id":"","override-preset":false,"main-bg-color":"#ffffff","header-color":{"color":"#292929","alpha":1},"top-header-color":{"color":"#000000","alpha":1},"submenu-bg-color":{"color":"#292929","alpha":1},"catnav-bg-color":{"color":"#f9f9f9","alpha":1},"sidebar-bg-color":{"color":"#fff","alpha":1},"sidebar-widget-bg-color":{"color":"#f9f9f9","alpha":1},"fixed-sidebar-bg-color":{"color":"#292929","alpha":1},"footer-bg-color":{"color":"#292929","alpha":1},"footer-copy-bg-color":{"color":"#000000","alpha":1},"body-text-color":"#000000","hyperlink-text-color":"#000000","hyperlink-text-hover-color":"#000000","paragraph-color":"#000000","header-top-color":"#eeeeee","header-contact-color":"#ffffff","navigation-menu-color":"#ffffff","navigation-menu-hover-color":"#eeeeee","active-menu-bg-color":"#dddddd","catnav-color":"#ffffff","sidebar-widget-title-color":"#999999","footer-widget-title-color":"#7c7c7c","footer-text-color":"#29353d","footer-copy-text-color":"#ccc","footer-socials-color":"#ffffff","share_names":"facebook,pinterest,googleplus,twitter,linkedin","show_gallery_header":true,"gallery_home_text":"[springbook_logo image=\"\"]\n<div class=\"hero-quote fl-wrap\">\n    <p>\" A super cool template for bloggers, photographers and travelers \"<\/p>\n<\/div>","gallery_header_video":"","gallery_header_image":{"url":"'.addcslashes( esc_url( home_url( '/' ) ) , '/').'wp-content\/themes\/springbook\/images\/bg\/10.jpg"},"gallery_home_text_intro":"<h3 class=\"big-title\">Gallery 3 columns<\/h3>\n<div class=\"separator img-separator\"><img src=\"'.addcslashes( esc_url( home_url( '/' ) ) , '/').'wp-content\/themes\/springbook\/images\/sep-blk.png\"  alt=\"\"><\/div>","gallery_layout":"nosidebar","gallery_column":"three","gallery_show_filter":true,"gallery_filter_orderby":"name","gallery_filter_order":"ASC","gallery_posts_per_page":"11","gallery_archive_orderby":"date","gallery_archive_order":"DESC","gallery_pad":"s20","gallery_show_title":true,"gallery_show_pagination":false,"gallery_pagination_style":"pagination","gallery_show_loadmore":true,"gallery_lm_items":"2","gallery_enable_gallery":false,"gal_date_single":true,"gal_title_single":true,"gal_cats":true,"gal_featured":false,"gal_gallery":true,"gal_show_like_post":true,"gal_show_views":true,"gal_comments":true,"gal_navigation":false,"gal_single_nav_same_term":false,"gal_list_link":"","gal_author_single":true,"show_blog_header":true,"show_logo_in_blog_header":true,"blog_home_text_intro":"<p>\" A super cool template for bloggers, photographers and travelers \"<\/p>","blog_header_video":"","blog_header_image":{"url":"'.addcslashes( esc_url( home_url( '/' ) ) , '/').'wp-content\/themes\/springbook\/images\/bg\/10.jpg"},"blog_layout":"right_sidebar","blog_show_featured_posts":false,"blog-grid-style":"masonry","blog-grid-columns":"two","pagination_style":"style1","blog_list_show_format":false,"blog_author":true,"blog_date":true,"blog_cats":true,"blog_tags":false,"blog_comments":true,"blog_show_like_post":true,"blog_show_views":true,"blog_single_title":true,"blog_featured_single":true,"blog_date_single":true,"blog_cats_single":true,"blog_tags_single":true,"blog_author_single":true,"blog_single_related":true,"blog_related_count":"3","blog_single_navigation":true,"blog_single_nav_same_term":false,"blog_list_link":"","bg404":{"url":"'.addcslashes( esc_url( home_url( '/' ) ) , '/').'wp-content\/themes\/springbook\/images\/bg\/12.jpg"},"404_intro":"<p>The page you are looking for has not been found. <br> Try to searching something else<\/p>","show_404_search":true,"use_cus_css_codes":true,"custom-css":"","custom-css-medium":"","custom-css-tablet":"","custom-css-mobile":"","mailchimp_shortcode":"","body-font":"","hyperlink-font":"","hyperlink-hover-font":"","paragraph-font":"","header-font":"","springbook-first-font":"","springbook-second-font":"","springbook-third-font":"","springbook-navigation-font":"","springbook-navigation-hover-font":"","springbook-navigation-active-font":"","redux_options_object":"","redux_import_export":"","REDUX_last_saved":1477302874,"REDUX_LAST_SAVE":1477302874}';

            $springbook_options = json_decode( $springbook_default_options ,true);
        }
        /*else{
            var_dump( json_encode( $springbook_options ) );die;
        }*/
    }
}
add_action('after_setup_theme', 'springbook_set_default_options', 1);

if(!function_exists('springbook_get_thumbnail_size')){
    function springbook_get_thumbnail_size(){
        global $springbook_options;
        $sizes = array();

        
        //Hero Header thumbnail
        if(isset($springbook_options['hero_header_thumb'])){
            $sizes['herothumb'] = array();
            $sizes['herothumb']['width'] = isset($springbook_options['hero_header_thumb']['width']) ? $springbook_options['hero_header_thumb']['width'] : '9999';
            $sizes['herothumb']['height'] = isset($springbook_options['hero_header_thumb']['height']) ? $springbook_options['hero_header_thumb']['height'] : '9999';
            $sizes['herothumb']['hard_crop'] = isset($springbook_options['hero_header_thumb']['hard_crop']) ? $springbook_options['hero_header_thumb']['hard_crop'] : 1;
        }else{
            $sizes['herothumb'] = array(
                'width' => '9999',
                'height' => '9999',
                'hard_crop' => 1,
            );
        }

        //Blog thumbnail
        if(isset($springbook_options['blog_post_thumb'])){
            $sizes['postthumb'] = array();
            $sizes['postthumb']['width'] = isset($springbook_options['blog_post_thumb']['width']) ? $springbook_options['blog_post_thumb']['width'] : '328';
            $sizes['postthumb']['height'] = isset($springbook_options['blog_post_thumb']['height']) ? $springbook_options['blog_post_thumb']['height'] : '217';
            $sizes['postthumb']['hard_crop'] = isset($springbook_options['blog_post_thumb']['hard_crop']) ? $springbook_options['blog_post_thumb']['hard_crop'] : 1;
        }else{
            $sizes['postthumb'] = array(
                'width' => '328',
                'height' => '217',
                'hard_crop' => 1,
            );
        }

        
        //Blog single thumbnail
        if(isset($springbook_options['blog_single_post_thumb'])){
            $sizes['postsingthumb'] = array();
            $sizes['postsingthumb']['width'] = isset($springbook_options['blog_single_post_thumb']['width']) ? $springbook_options['blog_single_post_thumb']['width'] : '9999';
            $sizes['postsingthumb']['height'] = isset($springbook_options['blog_single_post_thumb']['height']) ? $springbook_options['blog_single_post_thumb']['height'] : '9999';
            $sizes['postsingthumb']['hard_crop'] = isset($springbook_options['blog_single_post_thumb']['hard_crop']) ? $springbook_options['blog_single_post_thumb']['hard_crop'] : 1;
        }else{
            $sizes['postsingthumb'] = array(
                'width' => '9999',
                'height' => '9999',
                'hard_crop' => 1,
            );
        }

        //Related post size
        if(isset($springbook_options['related_post_thumb'])){
            $sizes['postrelated'] = array();
            $sizes['postrelated']['width'] = isset($springbook_options['related_post_thumb']['width']) ? $springbook_options['related_post_thumb']['width'] : '9999';
            $sizes['postrelated']['height'] = isset($springbook_options['related_post_thumb']['height']) ? $springbook_options['related_post_thumb']['height'] : '9999';
            $sizes['postrelated']['hard_crop'] = isset($springbook_options['related_post_thumb']['hard_crop']) ? $springbook_options['related_post_thumb']['hard_crop'] : 1;
        }else{
            $sizes['postrelated'] = array(
                'width' => '9999',
                'height' => '9999',
                'hard_crop' => 1,
            );
        }

        //Popup Images Gallery Grid
        if(isset($springbook_options['gallery_grid_thumb'])){
            $sizes['galgrid'] = array();
            $sizes['galgrid']['width'] = isset($springbook_options['gallery_grid_thumb']['width']) ? $springbook_options['gallery_grid_thumb']['width'] : '9999';
            $sizes['galgrid']['height'] = isset($springbook_options['gallery_grid_thumb']['height']) ? $springbook_options['gallery_grid_thumb']['height'] : '9999';
            $sizes['galgrid']['hard_crop'] = isset($springbook_options['gallery_grid_thumb']['hard_crop']) ? $springbook_options['gallery_grid_thumb']['hard_crop'] : 1;
        }else{
            $sizes['galgrid'] = array(
                'width' => '9999',
                'height' => '9999',
                'hard_crop' => 1,
            );
        }

        //Gallery Masonry
        if(isset($springbook_options['gallery_masonry_thumb'])){
            $sizes['galma'] = array();
            $sizes['galma']['width'] = isset($springbook_options['gallery_masonry_thumb']['width']) ? $springbook_options['gallery_masonry_thumb']['width'] : '9999';
            $sizes['galma']['height'] = isset($springbook_options['gallery_masonry_thumb']['height']) ? $springbook_options['gallery_masonry_thumb']['height'] : '9999';
            $sizes['galma']['hard_crop'] = isset($springbook_options['gallery_masonry_thumb']['hard_crop']) ? $springbook_options['gallery_masonry_thumb']['hard_crop'] : 1;
        }else{
            $sizes['galma'] = array(
                'width' => '9999',
                'height' => '9999',
                'hard_crop' => 1,
            );
        }

        

        return $sizes;
    }
}

/**
 * Register widget area.
 *
 * @since Springbook 1.0
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */
function springbook_register_sidebars() {


    register_sidebar(
        array(
            'name' => esc_html__('Categories Nav', 'springbook'), 
            'id' => 'cat_nav_widget', 
            'description' => esc_html__('Appears on top of blog content', 'springbook'), 
            'before_widget' => '<div id="%1$s" class="cat-nav-widget %2$s">', 
            'after_widget' => '</div>', 
            'before_title' => '<h3 class="widget-title">', 
            'after_title' => '</h3>',
        )
    );
    
    register_sidebar(
        array(
            'name' => esc_html__('Main Sidebar', 'springbook'), 
            'id' => 'sidebar-1', 
            'description' => esc_html__('Appears in the sidebar section of the site.', 'springbook'), 
            'before_widget' => '<div id="%1$s" class="widget main-sidebar-widget %2$s">', 
            'after_widget' => '</div>', 
            'before_title' => '<h3>', 
            'after_title' => '</h3>',
        )
    );

    register_sidebar(
        array(
            'name' => esc_html__('Fixed Sidebar', 'springbook'), 
            'id' => 'fixed_sidebar_widget', 
            'description' => esc_html__('Appears in left fixed sidebar.', 'springbook'), 
            'before_widget' => '<div id="%1$s" class="widget fixed-sidebar-widget %2$s">', 
            'after_widget' => '</div>', 
            'before_title' => '<h3>', 
            'after_title' => '</h3>',
        )
    );
    
    
    register_sidebar(
        array(
            'name' => esc_html__('After Blog Widget', 'springbook'), 
            'id' => 'after_blog_widget', 
            'description' => esc_html__('Appears bellow blog content', 'springbook'), 
            'before_widget' => '<div id="%1$s" class="after-blog-widget %2$s">', 
            'after_widget' => '</div>', 
            'before_title' => '<h3 class="widget-title">', 
            'after_title' => '</h3>',
        )
    );
    register_sidebar(
        array(
            'name' => esc_html__('Footer Columns Widget', 'springbook'), 
            'id' => 'footer_columns_widget', 
            'description' => esc_html__('Appears above the footer copyright content.', 'springbook'), 
            'before_widget' => '<div class="footer-cols footer-widget-inner ' . springbook_slbd_count_widgets('footer_columns_widget') . '"><div id="%1$s" class="footer-widget %2$s">', 
            'after_widget' => '</div></div>', 
            'before_title' => '<h3 class="footer-widget-title">', 
            'after_title' => '</h3>',
        )
    );

    register_sidebar(
        array(
            'name' => esc_html__('Footer Minimal Style Widget', 'springbook'), 
            'id' => 'footer_minimal_widget', 
            'description' => esc_html__('Appears above the footer copyright content.', 'springbook'), 
            'before_widget' => '<div class="footer-minimal-widgets"><div id="%1$s" class="footer-widget %2$s">', 
            'after_widget' => '</div></div>', 
            'before_title' => '<h3 class="footer-widget-title">', 
            'after_title' => '</h3>',
        )
    );

    
}

add_action('widgets_init', 'springbook_register_sidebars');

if(!function_exists('springbook_slbd_count_widgets')){
   /**
     * Count number of widgets in a sidebar
     * Used to add classes to widget areas so widgets can be displayed one, two, three or four per row
     */
    function springbook_slbd_count_widgets($sidebar_id) {
        
        // If loading from front page, consult $_wp_sidebars_widgets rather than options
        // to see if wp_convert_widget_settings() has made manipulations in memory.
        global $_wp_sidebars_widgets;
        if (empty($_wp_sidebars_widgets)):
            $_wp_sidebars_widgets = get_option('sidebars_widgets', array());
        endif;
        
        $sidebars_widgets_count = $_wp_sidebars_widgets;
        
        if (isset($sidebars_widgets_count[$sidebar_id])):
            $widget_count = count($sidebars_widgets_count[$sidebar_id]);
            $widget_classes = 'widget-count-' . count($sidebars_widgets_count[$sidebar_id]);
            if ($widget_count % 6 == 0 && $widget_count >= 6):
                
                // Six widgets er row if there are exactly four or more than six
                $widget_classes.= ' col-md-2';
            elseif ($widget_count % 4 == 0 && $widget_count >= 4):
                
                // Four widgets er row if there are exactly four or more than six
                $widget_classes.= ' col-md-3';
            elseif ($widget_count % 3 == 0 && $widget_count >= 3):
                
                // Three widgets per row if there's three or more widgets
                $widget_classes.= ' col-md-4';
            elseif (2 == $widget_count):
                
                // Otherwise show two widgets per row
                $widget_classes.= ' col-md-6';
            elseif (1 == $widget_count):
                
                // Otherwise show two widgets per row
                $widget_classes.= ' col-md-12';
            endif;
            
            return $widget_classes;
        endif;
    } 
}

if ( ! function_exists( 'springbook_google_fonts_url' ) ) {
    /**
     * Register Google fonts.
     *
     * @return string Google fonts URL for the theme.
     */
    function springbook_google_fonts_url() {
        $fonts_url = '';
        $fonts     = array();
        $subsets   = 'latin,latin-ext';

        /* translators: If there are characters in your language that are not supported by this font, translate this to 'off'. Do not translate into your own language. */
        if ( 'off' !== esc_html_x( 'on', 'Droid Serif font: on or off', 'springbook' ) ) {
            $fonts[] = 'Droid Serif:400,400italic,700,700italic';
        }

        /* translators: If there are characters in your language that are not supported by this font, translate this to 'off'. Do not translate into your own language. */
        if ( 'off' !== esc_html_x( 'on', 'Montserrat font: on or off', 'springbook' ) ) {
            $fonts[] = 'Montserrat:400,700';
        }

        /* translators: If there are characters in your language that are not supported by this font, translate this to 'off'. Do not translate into your own language. */
        if ( 'off' !== esc_html_x( 'on', 'Raleway font: on or off', 'springbook' ) ) {
            $fonts[] = 'Raleway:400,900,800,700,600,400italic,500,500italic,600italic,300';
        }

        if ( $fonts ) {
            $fonts_url = add_query_arg( array(
                'family' => urlencode( implode( '|', $fonts ) ),
                'subset' => urlencode( $subsets ),
            ), 'https://fonts.googleapis.com/css' );
        }

        return $fonts_url;
    }

}

/**
 * Enqueue scripts and styles.
 *
 * @since Springbook 1.0
 */

if (!function_exists('springbook_scripts_styles')) {
    function springbook_scripts_styles() {
        global $springbook_options;
        if (is_singular() && comments_open() && get_option('thread_comments')) {
            wp_enqueue_script('comment-reply');
        }
        wp_enqueue_script("springbookplugins-js", get_template_directory_uri() . "/js/plugins.js", array('jquery'), false, true);

        wp_register_script( "springbookinstafeed-js", get_template_directory_uri() . "/js/instafeed.js", array('jquery'), false, true );

        wp_enqueue_script("springbookscripts-js", get_stylesheet_directory_uri() . "/js/scripts.js", array(), false, true);
        $springbook_datas = array();
        $springbook_datas['fsb_width'] = $springbook_options['fixed_sidebar_width'];
        wp_localize_script( 'springbookscripts-js', '_springbook', $springbook_datas );
        wp_enqueue_style('springbookplugins-css', get_template_directory_uri() . '/css/plugins.css');

        wp_enqueue_style('springbookgoogle-fonts', springbook_google_fonts_url());

        wp_enqueue_style('springbooktheme-style', get_stylesheet_uri(), array(), '2016-04-23');
        wp_enqueue_style('springbookcustom-css', get_stylesheet_directory_uri() . '/css/custom.css');
        if ($springbook_options['override-preset']) {
            $inline_style = springbook_overridestyle();
            if (!empty($inline_style)) {
                wp_add_inline_style('springbookcustom-css', $inline_style);
            }
        }
        if($springbook_options['use_cus_css_codes']){
            $inline_custom_style = trim($springbook_options['custom-css']);

            if(isset($springbook_options['custom-css-medium']) && !empty($springbook_options['custom-css-medium'])){
                $inline_custom_style .= '@media only screen and  (max-width: 1200px){'.trim($springbook_options['custom-css-medium']).'}';
            }
            if(isset($springbook_options['custom-css-tablet']) && !empty($springbook_options['custom-css-tablet'])){
                $inline_custom_style .= '@media only screen and  (max-width: 991px){'.trim($springbook_options['custom-css-tablet']).'}';
            }
            if(isset($springbook_options['custom-css-mobile']) && !empty($springbook_options['custom-css-mobile'])){
                $inline_custom_style .= '@media only screen and  (max-width: 767px){'.trim($springbook_options['custom-css-mobile']).'}';
            }
            
            if (!empty($inline_custom_style)) {
                wp_add_inline_style('springbookcustom-css', springbook_stripWhitespace($inline_custom_style) );
            }
        }
        
    }
}
add_action('wp_enqueue_scripts', 'springbook_scripts_styles');

/**
 * Enqueue admin scripts and styles.
 *
 * @since Springbook 1.0
 */

if (!function_exists('springbook_enqueue_admin_scripts')) {
    function springbook_enqueue_admin_scripts() {
        
        wp_enqueue_style('springbookadmin-styles', get_template_directory_uri() . '/inc/assets/admin_styles.css');
    }
}
add_action('admin_enqueue_scripts', 'springbook_enqueue_admin_scripts');



/**
 * Modify menu link class attribute
 *
 * @since Springbook 1.0
 */
$springbook_menu_link_class = array();

add_filter('nav_menu_css_class', 'springbook_nav_menu_css_class_func', 10, 2);

function springbook_nav_menu_css_class_func($classes, $item) {
    global $springbook_menu_link_class;
    $springbook_menu_link_class = array();
    $ajax_menu = array_search("ajax", $classes);
    if ($ajax_menu !== false) {
        $springbook_menu_link_class[] = 'ajax';
        unset($classes['ajax']);
    }
    $external_menu = array_search("external", $classes);
    if ($external_menu !== false) {
        $springbook_menu_link_class[] = 'external';
    }
    //scroll menu for multipage menu
    $menu_scroll_link_menu = array_search("menu-scroll-link", $classes);
    if ($menu_scroll_link_menu !== false) {
        $springbook_menu_link_class[] = 'menu-scroll-link';
    }
    $current_menu = array_search("current-menu-item", $classes);
    if ($current_menu !== false) {
        $springbook_menu_link_class[] = 'act-link';
    }
    $current_menu_ancestor = array_search("current-menu-ancestor", $classes);
    if ($current_menu_ancestor !== false) {
        $springbook_menu_link_class[] = 'ancestor-act-link';
    }
    $current_menu_parent = array_search("current-menu-parent", $classes);
    if ($current_menu_parent !== false) {
        $springbook_menu_link_class[] = 'parent-act-link';
    }
    return $classes;
}

add_filter('nav_menu_link_attributes', 'springbook_nav_menu_link_attributes_func', 10, 3);

function springbook_nav_menu_link_attributes_func($atts, $item, $args) {
    global $springbook_menu_link_class;
    if (!empty($springbook_menu_link_class)) {
        $atts['class'] = implode(" ", $springbook_menu_link_class);
    }
    
    return $atts;
}

/**
 * Change posts per page setting for portfolio archive pages.
 *
 * @since Springbook 1.0
 */
function springbook_pagesize($query) {
    global $springbook_options;
    
    if (is_admin() || !$query->is_main_query()) return;
    
    if (is_post_type_archive('spbgallery') || is_tax('spbgallery_cat')) {
        
        // Display 50 posts for a custom post type called 'gallery'
        if (isset($springbook_options['gallery_posts_per_page'])) {
            $query->set('posts_per_page', $springbook_options['gallery_posts_per_page']);
        }
        // oder gallery posts
        if (isset($springbook_options['gallery_archive_orderby'])) {
            $query->set('orderby', $springbook_options['gallery_archive_orderby']);
        }
        if (isset($springbook_options['gallery_archive_order'])) {
            $query->set('order', $springbook_options['gallery_archive_order']);
        }
        return;
    }

    
}
add_action('pre_get_posts', 'springbook_pagesize', 1);

if(!function_exists('springbook_breadcrumbs')){
    function springbook_breadcrumbs() {
               
        // Settings
        $separator          = esc_html__('/','springbook');//'&gt;';
        $breadcrums_id      = 'breadcrumb';
        $breadcrums_class   = 'main-breadcrumb';
        $home_title         = esc_html__('Home','springbook');
        $blog_title         = esc_html__('Blog','springbook');
         
        // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
        $custom_taxonomy    = '';

        $custom_post_types = array(
                                'spbgallery' => esc_html_x('Gallery','gallery archive in breadcrumb','springbook'),
                                'portfolio' => esc_html_x('Portfolio','portfolio archive in breadcrumb','springbook'),
                                'springbook_member' => esc_html_x('Team','Team archive in breadcrumb','springbook'),
                            );
          
        // Get the query & post information
        global $post,$wp_query;
          
        // Do not display on the homepage
        if ( !is_front_page() ) {
          
            // Build the breadcrums
            echo '<ul id="' . esc_attr($breadcrums_id ) . '" class="' . esc_attr($breadcrums_class ) . '">';
              
            // Home page
            echo '<li class="item-home"><a class="bread-link bread-home" href="' . esc_url(get_home_url(null,'/') ) . '" title="' . esc_attr($home_title ) . '">' . esc_attr($home_title ) . '</a></li>';

            if(is_home()){
                // Blog page
                echo '<li class="item-current item-blog"><strong class="bread-current item-blog">' . esc_attr($blog_title ) . '</strong></li>';
            }
              
            if ( is_archive() && !is_tax() ) {

                // If post is a custom post type
                $post_type = get_post_type();

                if($post_type && array_key_exists($post_type, $custom_post_types)){
                    echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><strong class="bread-current bread-archive">' . $custom_post_types[$post_type] . '</strong></li>';
                }else{
                     echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . get_the_archive_title() . '</strong></li>';
                }
                 
            } else if ( is_archive() && is_tax() ) {
                 
                // If post is a custom post type
                $post_type = get_post_type();
                 
                // If it is a custom post type display name and link
                if($post_type && $post_type != 'post') {
                     
                    $post_type_object = get_post_type_object($post_type);
                    $post_type_archive = get_post_type_archive_link($post_type);
                 
                    echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . esc_url($post_type_archive ) . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                 
                }
                 
                $custom_tax_name = get_queried_object()->name;
                echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';
                 
            } else if ( is_single() ) {
                
                // If post is a custom post type
                $post_type = get_post_type();
                $last_category = '';
                // If it is a custom post type (not support custom taxonomy) display name and link
                if( !in_array( $post_type, array('post','portfolios') ) ) {
                     
                    $post_type_object = get_post_type_object($post_type);
                    $post_type_archive = get_post_type_archive_link($post_type);

                    if(array_key_exists($post_type, $custom_post_types)){
                        echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . esc_url($post_type_archive ) . '" title="' . $custom_post_types[$post_type] . '">' . $custom_post_types[$post_type] . '</a></li>';
                    }else{
                        echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . esc_url($post_type_archive ) . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                    }
                    
                    echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                }elseif($post_type == 'post'){
                    // Get post category info
                    $category = get_the_category();
                     
                    // Get last category post is in
                    
                    if($category){
                        $last_cateogries = array_values($category);
                        $last_category = end($last_cateogries);
                     
                        // Get parent any categories and create array
                        $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                        $cat_parents = explode(',',$get_cat_parents);
                         
                        // Loop through parent categories and store in variable $cat_display
                        $cat_display = '';
                        foreach($cat_parents as $parents) {
                            $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                        }
                    }
                    
                    if(!empty($last_category)) {
                        echo wp_kses_post($cat_display );
                        echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                         
                    // Else if post is in a custom taxonomy
                    }
                }
                    
                     
                // If it's a custom post type within a custom taxonomy
                if(empty($last_category) && !empty($custom_taxonomy)) {
                    $custom_taxonomy_arr = explode(",", $custom_taxonomy) ;
                    foreach ($custom_taxonomy_arr as $key => $custom_taxonomy_val) {
                        $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy_val );
                        if($taxonomy_terms && !($taxonomy_terms instanceof WP_Error) ){
                            $cat_id         = $taxonomy_terms[0]->term_id;
                            $cat_nicename   = $taxonomy_terms[0]->slug;
                            $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy_val);
                            $cat_name       = $taxonomy_terms[0]->name;

                            if(!empty($cat_id)) {
                         
                                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . esc_url($cat_link ) . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
                                
                                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                             
                            }
                        }

                     } 
                    
                  
                }
                 
                
                 
            } else if ( is_category() ) {
                  
                // Category page
                echo '<li class="item-current item-cat-' . $category[0]->term_id . ' item-cat-' . $category[0]->category_nicename . '"><strong class="bread-current bread-cat-' . $category[0]->term_id . ' bread-cat-' . $category[0]->category_nicename . '">' . $category[0]->cat_name . '</strong></li>';
                  
            } else if ( is_page() ) {
                  
                // Standard page
                if( $post->post_parent ){

                    $parents = '';
                      
                    // If child page, get parents 
                    $anc = get_post_ancestors( $post->ID );
                      
                    // Get parents in the right order
                    $anc = array_reverse($anc);
                      
                    // Parent page loop
                    foreach ( $anc as $ancestor ) {
                        $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . esc_url(get_permalink($ancestor) ) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                        
                    }
                      
                    // Display parent pages
                    echo wp_kses_post($parents );
                      
                    // Current page
                    echo '<li class="item-current item-page item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                      
                } else {
                      
                    // Just display current page if not parents
                    echo '<li class="item-current item-page item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                      
                }
                  
            } else if ( is_tag() ) {
                  
                // Tag page
                  
                // Get tag information
                $term_id = get_query_var('tag_id');
                // $taxonomy = 'post_tag';
                $args = array(
                    'taxonomy'=> 'post_tag',
                    'include' => $term_id
                );
                
                $terms = get_terms($args );
                  
                // Display the tag name
                echo '<li class="item-current item-tag-' . $terms[0]->term_id . ' item-tag-' . $terms[0]->slug . '"><strong class="bread-current bread-tag-' . $terms[0]->term_id . ' bread-tag-' . $terms[0]->slug . '">' . $terms[0]->name . '</strong></li>';
              
            } elseif ( is_day() ) {
                  
                // Day archive
                  
                // Year link
                echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . esc_html__(' Archives','springbook').'</a></li>';
                
                  
                // Month link
                echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . esc_html__(' Archives','springbook').'</a></li>';
                
                  
                // Day display
                echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') .  esc_html__(' Archives','springbook').'</strong></li>';
                  
            } else if ( is_month() ) {
                  
                // Month Archive
                  
                // Year link
                echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . esc_html__(' Archives','springbook').'</a></li>';
                
                  
                // Month display
                echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . esc_html__(' Archives','springbook').'</strong></li>';
                  
            } else if ( is_year() ) {
                  
                // Display year archive
                echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . esc_html__(' Archives','springbook').'</strong></li>';
                  
            } else if ( is_author() ) {
                  
                // Auhor archive
                  
                // Get the author information
                global $author;
                $userdata = get_userdata( $author );
                  
                // Display author name
                echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' .  esc_html__(' Author: ','springbook') . $userdata->display_name . '</strong></li>';
              
            } else if ( get_query_var('paged') ) {
                  
                // Paginated archives
                echo '<li class="item-current item-current-' . get_query_var('paged') . '"><a href="#" class="bread-current bread-current-' . get_query_var('paged') . '" title="'.esc_html__('Page','springbook') . get_query_var('paged') . '">'.esc_html__('Page','springbook') . ' ' . get_query_var('paged') . '</a></li>';
                  
            } else if ( is_search() ) {
              
                // Search results page
                echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="'.esc_html__('Search results for: ','springbook') . get_search_query() . '">'.esc_html__('Search results for: ','springbook') . get_search_query() . '</strong></li>';
              
            } elseif ( is_404() ) {
                  
                // 404 page
                echo '<li>' . esc_html__('Error 404','springbook') . '</li>';
            }
          
            echo '</ul>';
              
        }
          
    }
}


/**
 * Theme pagination
 *
 * @since Springbook 1.0
 */
if (!function_exists('springbook_pagination')) {
    function springbook_pagination($pages = '', $sec_wrap = false) {
        global $wp_query, $wp_rewrite;
        $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
        if ($pages == '') {
            
            $pages = $wp_query->max_num_pages;
            if (!$pages) {
                $pages = 1;
            }
        }
        $pagination = array('base' => str_replace(999999999, '%#%', get_pagenum_link(999999999)), 'format' => '', 'current' => max(1, get_query_var('paged')), 'total' => $pages, 'prev_text' => wp_kses(__('<i class="fa fa-caret-left" aria-hidden="true"></i>','springbook'),array('i'=>array('class'=>array(),),) ) , 'next_text' => wp_kses(__('<i class="fa fa-caret-right" aria-hidden="true"></i>','springbook'),array('i'=>array('class'=>array(),),) ), 'type' => 'plain', 'end_size' => 3, 'mid_size' => 3);
        $return = paginate_links($pagination);
        $return = str_replace(array('page-numbers', 'next', 'prev', 'current'), array('blog-page', 'nextposts-link arrow-nav rig-ar-nav', 'prevposts-link arrow-nav lef-ar-nav', 'current-page'), $return);
        if (!empty($return)) {
            if ($sec_wrap) echo '<section class="custom_folio_pagi">';
            echo '<div class="clearfix"></div><div class="vertical-separator"></div><div class="page_nav main-page-nav pagi_style1">' . $return . '</div>';
            if ($sec_wrap) echo '</section>';
        }
    }
}



/**
 * Theme pagination
 *
 * @since Springbook 1.0
 */
if (!function_exists('springbook_pagination_style2')) {
    function springbook_pagination_style2($pages = '', $sec_wrap = false) {
        global $wp_query, $wp_rewrite;
        $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
        if ($pages == '') {
            
            $pages = $wp_query->max_num_pages;
            if (!$pages) {
                $pages = 1;
            }
        }
        $pagination = array('base' => str_replace(999999999, '%#%', get_pagenum_link(999999999)), 'format' => '', 'current' => max(1, get_query_var('paged')), 'total' => $pages,  'prev_text' => wp_kses(__('<i class="fa fa-angle-double-left"></i> Newer Posts','springbook'),array('i'=>array('class'=>array(),),) ) , 'next_text' => wp_kses(__('Older Posts <i class="fa fa-angle-double-right"></i>','springbook'),array('i'=>array('class'=>array(),),) ), 'type' => 'plain', 'end_size' => 3, 'mid_size' => 3);
        $return = paginate_links($pagination);
        $return = str_replace(array('page-numbers', 'next', 'prev', 'current'), array('blog-page', 'nextposts-link older', 'prevposts-link newer', 'current-page'), $return);
        if (!empty($return)) {
            if ($sec_wrap) echo '<section class="custom_folio_pagi">';
            echo '<div class="clearfix"></div><div class="pagination pagi_style2 fl-wrap">' . $return . '</div>';
            if ($sec_wrap) echo '</section>';
        }
    }
}

/**
 * Theme pagination
 *
 * @since Springbook 1.0
 */
if (!function_exists('springbook_pagination_style3')) {
    function springbook_pagination_style3($pages = '', $sec_wrap = false) {
        global $wp_query, $wp_rewrite;
        $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
        if ($pages == '') {
            
            $pages = $wp_query->max_num_pages;
            if (!$pages) {
                $pages = 1;
            }
        }
        $pagination = array('base' => str_replace(999999999, '%#%', get_pagenum_link(999999999)), 'format' => '', 'current' => max(1, get_query_var('paged')), 'total' => $pages, 'prev_text' => wp_kses(__('<i class="fa fa-chevron-left"></i>','springbook'),array('i'=>array('class'=>array(),),) ) , 'next_text' => wp_kses(__('<i class="fa fa-chevron-right"></i>','springbook'),array('i'=>array('class'=>array(),),) ), 'type' => 'plain', 'end_size' => 3, 'mid_size' => 3);
        $return = paginate_links($pagination);
        $return = str_replace(array('page-numbers', 'next', 'prev', 'current'), array('blog-page', 'nextposts-link', 'prevposts-link', 'current-page'), $return);
        if (!empty($return)) {
            if ($sec_wrap) echo '<section class="custom_folio_pagi">';
            echo '<div class="clearfix"></div><div class="pagination2 pagi_style3 fl-wrap">' . $return . '</div>';
            if ($sec_wrap) echo '</section>';
        }
    }
}


/**
 * Pagination for Portfolio page templates
 *
 * @since Springbook 1.0
 */
if (!function_exists('springbook_custom_pagination')) {
    function springbook_custom_pagination($pages = '', $range = 2, $current_query = '', $always_next_prev = false) {

        $showitems = ($range * 2) + 1;
        
        if ($current_query == '') {
            global $paged;
            if (empty($paged)) $paged = 1;
        } 
        else {
            $paged = $current_query->query_vars['paged'];
        }
        
        if ($pages == '') {
            if ($current_query == '') {
                global $wp_query;
                $pages = $wp_query->max_num_pages;
                if (!$pages) {
                    $pages = 1;
                }
            } 
            else {
                $pages = $current_query->max_num_pages;
            }
        }
        
        if (1 < $pages) {

            echo '<div class="clearfix"></div><div class="vertical-separator"></div>';
            echo '<div class="page_nav main-page-nav pagi_style1">';
            
            if ($paged > 1){
                echo '<a href="' . get_pagenum_link($paged - 1) . '" class="prevposts-link arrow-nav lef-ar-nav"><i class="fa fa-caret-left" aria-hidden="true"></i></a>';
            }elseif($always_next_prev){
                echo '<a href="' . get_pagenum_link($paged) . '" class="prevposts-link arrow-nav lef-ar-nav"><i class="fa fa-caret-left" aria-hidden="true"></i></a>';
            }
            echo '<ul>';
            for ($i = 1; $i <= $pages; $i++) {
                
                if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems)) {
                echo'<li>';
                    echo ($paged == $i) ? "<span class='blog-page current-page transition'>" . $i . "</span>" : "<a href='" . get_pagenum_link($i) . "' class='blog-page transition' >" . $i . "</a>";
                echo '</li>';
                }
                
            }
            echo '</ul>';
            if ($paged < $pages){
                echo '<a href="' . get_pagenum_link($paged + 1) . '" class="nextposts-link arrow-nav rig-ar-nav"><i class="fa fa-caret-right" aria-hidden="true"></i></a>';
            }elseif($always_next_prev){
                echo '<a href="' . get_pagenum_link($paged) . '" class="nextposts-link arrow-nav rig-ar-nav"><i class="fa fa-caret-right" aria-hidden="true"></i></a>';
            }
            
            echo '</div>';

        }
    }
}

/**
 * Pagination for Portfolio page templates
 *
 * @since Springbook 1.0
 */
if (!function_exists('springbook_custom_pagination_style2')) {
    function springbook_custom_pagination_style2($pages = '', $range = 2, $current_query = '', $always_next_prev = false) {
        $showitems = ($range * 2) + 1;
        
        if ($current_query == '') {
            global $paged;
            if (empty($paged)) $paged = 1;
        } 
        else {
            $paged = $current_query->query_vars['paged'];
        }
        
        if ($pages == '') {
            if ($current_query == '') {
                global $wp_query;
                $pages = $wp_query->max_num_pages;
                if (!$pages) {
                    $pages = 1;
                }
            } 
            else {
                $pages = $current_query->max_num_pages;
            }
        }
        
        if (1 < $pages) {
            echo '<div class="pagination pagi_style2 fl-wrap">';
            
            if ($paged > 1){
                echo '<div class="newer"><a href="' . get_pagenum_link($paged - 1) . '" class="prevposts-link arrow-nav lef-ar-nav"><i class="fa fa-angle-double-left"></i> Newer Posts</a></div>';
            }
            elseif($always_next_prev){
                echo '<div class="newer"><a href="' . get_pagenum_link($paged) . '" class="prevposts-link arrow-nav lef-ar-nav"><i class="fa fa-angle-double-left"></i> Newer Posts</a></div>';
            }
            if ($paged < $pages){
                echo '<div class="older"><a href="' . get_pagenum_link($paged + 1) . '" class="nextposts-link arrow-nav rig-ar-nav">Older Posts <i class="fa fa-angle-double-right"></i></a></div>';
            }
            elseif($always_next_prev){
                echo '<div class="older"><a href="' . get_pagenum_link($paged) . '" class="nextposts-link arrow-nav rig-ar-nav">Older Posts <i class="fa fa-angle-double-right"></i></a></div>';
            }
            
            echo '</div>';
        }
    }
}
/**
 * Pagination for Portfolio page templates
 *
 * @since Springbook 1.0
 */
if (!function_exists('springbook_custom_pagination_style3')) {
    function springbook_custom_pagination_style3($pages = '', $range = 2, $current_query = '', $always_next_prev = false) {
        
        $showitems = ($range * 2) + 1;
        
        if ($current_query == '') {
            global $paged;
            if (empty($paged)) $paged = 1;
        } 
        else {
            $paged = $current_query->query_vars['paged'];
        }
        
        if ($pages == '') {
            if ($current_query == '') {
                global $wp_query;
                $pages = $wp_query->max_num_pages;
                if (!$pages) {
                    $pages = 1;
                }
            } 
            else {
                $pages = $current_query->max_num_pages;
            }
        }
        
        if (1 < $pages) {
            
            echo '<div class="pagination2 pagi_style3 fl-wrap">';
            
            if ($paged > 1){
                echo '<a href="' . get_pagenum_link($paged - 1) . '" class="prevposts-link arrow-nav lef-ar-nav"><i class="fa fa-chevron-left"></i></a>';
            }elseif($always_next_prev){
                echo '<a href="' . get_pagenum_link($paged) . '" class="prevposts-link arrow-nav lef-ar-nav"><i class="fa fa-chevron-left"></i></a>';
            }
            for ($i = 1; $i <= $pages; $i++) {
                
                if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems)) {
                    echo ($paged == $i) ? "<a href='#' class='blog-page current-page transition'>" . $i . "</a>" : "<a href='" . get_pagenum_link($i) . "' class='blog-page transition' >" . $i . "</a>";
                }
                
            }
            if ($paged < $pages){
                echo '<a href="' . get_pagenum_link($paged + 1) . '" class="nextposts-link arrow-nav rig-ar-nav"><i class="fa fa-chevron-right"></i></a>';
            }elseif($always_next_prev){
                echo '<a href="' . get_pagenum_link($paged) . '" class="nextposts-link arrow-nav rig-ar-nav"><i class="fa fa-chevron-right"></i></a>';
            }
            
            echo '</div>';
        }
    }
}


/**
 * Blog post nav
 *
 * @since Springbook 1.0
 */
if (!function_exists('springbook_post_nav')) {
    function springbook_post_nav() {
        global $post;
        global $springbook_options;
        if(!$springbook_options['blog_single_navigation'])
            return ;
?>
<div class="clearfix"></div>
<div class="page_nav main-page-nav single-nav">
<?php
$prev_post = get_adjacent_post( $springbook_options['blog_single_nav_same_term'], '', true );
if ( is_a( $prev_post, 'WP_Post' ) ) :
?>
    <a href="<?php echo get_permalink( $prev_post->ID ); ?>" class="arrow-nav lef-ar-nav" title="<?php echo get_the_title($prev_post->ID ); ?>"><i class="fa fa-angle-double-left" aria-hidden="true"></i> <?php esc_html_e('Previous Post','springbook' );?><?php /*echo get_the_title($prev_post->ID );*/ ?></a>
<?php endif ?>
<?php if( $springbook_options['blog_list_link'] != '' ) :?>
    <ul>
        <li><a href="<?php echo esc_url( $springbook_options['blog_list_link'] ); ?>"><i class="fa fa-th"></i></a></li>
    </ul>
<?php endif;?>
<?php
$next_post = get_adjacent_post( $springbook_options['blog_single_nav_same_term'], '', false );
if ( is_a( $next_post, 'WP_Post' ) ) :
?>
    <a href="<?php echo get_permalink( $next_post->ID ); ?>" class="arrow-nav rig-ar-nav" title="<?php echo get_the_title($next_post->ID ); ?>"><?php /*echo get_the_title($next_post->ID );*/ ?><?php esc_html_e('Next Post','springbook' );?> <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
<?php endif ?>
</div>
    <?php
// }
    }
}

/**
 * Gallery post nav
 *
 * @since Springbook 1.0
 */
if (!function_exists('springbook_gal_nav')) {
    function springbook_gal_nav() {
        global $post;
        global $springbook_options;
        if(!$springbook_options['gal_navigation'])
            return ;
?>
<div class="clearfix"></div>
<div class="page_nav main-page-nav single-nav">
<?php
$prev_post = get_adjacent_post( $springbook_options['gal_single_nav_same_term'], '', true, 'spbgallery_cat' );
if ( is_a( $prev_post, 'WP_Post' ) ) :
?>
    <a href="<?php echo get_permalink( $prev_post->ID ); ?>" class="arrow-nav lef-ar-nav" title="<?php echo get_the_title($prev_post->ID ); ?>"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
<?php endif ?>
<?php if( $springbook_options['gal_list_link'] != '' ) :?>
    <ul>
        <li><a href="<?php echo esc_url( $springbook_options['gal_list_link'] ); ?>"><i class="fa fa-th"></i></a></li>
    </ul>
<?php endif;?>
<?php
$next_post = get_adjacent_post( $springbook_options['gal_single_nav_same_term'], '', false, 'spbgallery_cat' );
if ( is_a( $next_post, 'WP_Post' ) ) :
?>
    <a href="<?php echo get_permalink( $next_post->ID ); ?>" class="arrow-nav rig-ar-nav" title="<?php echo get_the_title($next_post->ID ); ?>"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
<?php endif ?>
</div>
    <?php
// }
    }
}




/**
 * Custom comments list
 *
 * @since Springbook 1.0
 */
if (!function_exists('springbook_comments')) {
    function springbook_comments($comment, $args, $depth) {
        $GLOBALS['comment'] = $comment;
        extract($args, EXTR_SKIP);
        
        if ('div' == $args['style']) {
            $tag = 'div';
            $add_below = 'comment';
        } 
        else {
            $tag = 'li';
            $add_below = 'div-comment';
        }
?>
        <<?php
        echo esc_attr($tag); ?> <?php
        comment_class(empty($args['has_children']) ? 'media' : 'media parent') ?> id="comment-<?php
        comment_ID() ?>">
        <?php
        if ('div' != $args['style']): ?>
        <div id="div-comment-<?php
            comment_ID() ?>" class="thecomment">
        <?php
        endif; ?>

            <div class="author-img">
                <?php
        if ($args['avatar_size'] != 0) echo get_avatar($comment, $args['avatar_size']); ?>
            </div>

            <div class="comment-text">
                <span class="author"><?php echo get_comment_author_link($comment->comment_ID); ?></span>
                <?php comment_text(); ?>
                <span class="date"><?php echo get_comment_date(esc_html__('F j, Y - g:i a', 'springbook')); ?></span>
                <span class="reply">
                <?php comment_reply_link(array_merge($args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?>
                </span>
            </div>

            <?php if ($comment->comment_approved == '0'): ?>
                <em class="comment-awaiting-moderation aligncenter"><?php esc_html_e('Your comment is awaiting moderation.', 'springbook'); ?></em>
                <br />
            <?php endif; ?>
        
        <?php
        if ('div' != $args['style']): ?>
        </div> 
        <?php
        endif; ?>

    <?php
    }
}

/**
 * Modify tag cloud format
 *
 * @since Springbook 1.0
 */
function springbook_custom_tag_cloud_widget($args) {
    $args['format']     = 'list';
    $args['smallest']   = 10;
    $args['largest']    = 10;
    $args['unit']       = 'px';
     //ul with a class of wp-tag-cloud
    return $args;
}
add_filter('widget_tag_cloud_args', 'springbook_custom_tag_cloud_widget');


/**
 * Return attachment image link by using wp_get_attachment_image_src function
 *
 * @since Springbook 2.4
 */
function springbook_get_attachment_thumb_link( $id, $size = 'thumbnail', $icon = false ){
    $image_attributes = wp_get_attachment_image_src( $id, $size, $icon );
    if ( $image_attributes ) {
        return $image_attributes[0];
    }
    return '';
}

/** 
 * Filter the body_class for global configuration
 * 
 * @since 1.0.1
 */ 
// Apply filter
add_filter('body_class', 'springbook_body_classes');
if(!function_exists('springbook_body_classes')){
    function springbook_body_classes($classes) {
            $classes[] = 'springbook-body-classes';
            return $classes;
    }
}

/** 
 * Global variables fix
 * https://forums.envato.com/t/redux-framework-global-variable-issue/36739
 * @since Springbook 1.0
 */ 
if ( !function_exists('springbook_global_var') ) {
    function springbook_global_var($opt_1 = '', $opt_2 = '', $opt_check = false){
        global $springbook_options;
        if( $opt_check ) {
            if(isset($springbook_options[$opt_1][$opt_2])) {
                return $springbook_options[$opt_1][$opt_2];
            }
        } else {
            if(isset($springbook_options[$opt_1])) {
                return $springbook_options[$opt_1];
            }
        }
        
    }
}


if(!function_exists('springbook_get_template_part')){
    /**
     * Load a template part into a template
     *
     * Makes it easy for a theme to reuse sections of code in a easy to overload way
     * for child themes.
     *
     * Includes the named template part for a theme or if a name is specified then a
     * specialised part will be included. If the theme contains no {slug}.php file
     * then no template will be included.
     *
     * The template is included using require, not require_once, so you may include the
     * same template part multiple times.
     *
     * For the $name parameter, if the file is called "{slug}-special.php" then specify
     * "special".
      * For the var parameter, simple create an array of variables you want to access in the template
     * and then access them e.g. 
     * 
     * array("var1=>"Something","var2"=>"Another One","var3"=>"heres a third";
     * 
     * becomes
     * 
     * $var1, $var2, $var3 within the template file.
     *
     * @since 1.0.1
     *
     * @param string $slug The slug name for the generic template.
     * @param string $name The name of the specialised template.
     * @param array $vars The list of variables to carry over to the template
     * @author CTHthemes 
     * @ref http://www.zmastaa.com/2015/02/06/php-2/wordpress-passing-variables-get_template_part
     * @ref http://keithdevon.com/passing-variables-to-get_template_part-in-wordpress/
     */
    function springbook_get_template_part( $slug, $name = null, $vars = null ) {

        $template = "{$slug}.php";
        $name = (string) $name;
        if ( '' !== $name && ( file_exists( get_stylesheet_directory() ."/{$slug}-{$name}.php") || file_exists( get_template_directory() ."/{$slug}-{$name}.php") ) ) {
            $template = "{$slug}-{$name}.php";
        }

        extract($vars);
        include(locate_template($template));
    }
}



/**
* Add custom query vars for demo
*
* @since Springbook 1.0
*/
function springbook_query_vars_filter( $vars ){
  $vars[] = "hdcl";
  $vars[] = "ftcl";
  $vars[] = "ftst";
  return $vars;
}
add_filter( 'query_vars', 'springbook_query_vars_filter' );

/**
* Add custom post class
* @param array $classes An array of post classes.
* @param array $class   An array of additional classes added to the post.
* @param int   $post_id The post ID.
* @since Springbook 1.0
*/
    
function springbook_post_class_filter( $classes, $class, $post_id){
  if(!has_post_thumbnail()) {
    $classes[] = 'hasnot_post_thumbnail';
  }
  return $classes;
}
add_filter( 'post_class', 'springbook_post_class_filter', 10, 3 );

function springbook_password_protected_form() {
    global $post;
    $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
    $o = '<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" method="post" class="password-form">
    ' . esc_html__( "To view this protected post, enter the password below:" ,"springbook") . '
    <input name="post_password" id="' . $label . '" type="password" placeholder="'.esc_attr_x( 'Password', 'password input placeholder text','springbook' ).'"/><button class="password-form-submit"><i class="fa fa-eye"></i></button>
    </form>
    ';
    return $o;
}

add_filter( 'the_password_form', 'springbook_password_protected_form' );

/** 
 * Check for springbook_logo shortcode
 * @since Springbook 1.0
 * https://codex.wordpress.org/Function_Reference/get_shortcode_regex
 */ 
if ( !function_exists('springbook_check_logo_shortcode') ) {
    function springbook_check_logo_shortcode($content){

        if ( !shortcode_exists( 'springbook_logo' ) ) {

            $pattern = get_shortcode_regex(array('springbook_logo'));

            return preg_replace('/'. $pattern .'/s', '', $content );

        }else {
            return $content;
        }  
    }
}



/**
 * Custom meta box for page, post, portfolio...
 *
 * @since Springbook 1.0
 */
require_once get_template_directory() . '/cmb2/functions.php';

/**
 * Visual Composer plugin integration
 *
 * @since Springbook 1.0
 */
require_once get_template_directory() . '/inc/cth_for_vc.php';

/**
 * Theme custom style
 *
 * @since Springbook 1.0
 */
require_once get_template_directory() . '/inc/overridestyle.php';

/**
 * Taxonomy meta box
 *
 * @since Springbook 1.0
 */
require_once get_template_directory() . '/inc/cth_taxonomy_fields.php';
require_once get_template_directory() . '/inc/category_metabox_fields.php';
require_once get_template_directory() . '/inc/tag_metabox_fields.php';
require_once get_template_directory() . '/inc/spbgallery_cat_metabox_fields.php';

/**
 * Fixed icons menu
 *
 * @since Springbook 1.0
 */
require_once get_template_directory() . '/inc/springbook_fixedicons_navwalker.php';

/**
 * Custom elements for VC
 *
 * @since Springbook 1.0
 */
require_once get_template_directory() . '/vc_extend/vc_shortcodes.php';

/**
 * Implement Ajax calls
 *
 * @since Springbook 1.0
 */
require_once get_template_directory() . '/inc/ajax.php';

/**
 * Implement Post views
 *
 * @since Springbook 1.0
 */
require_once get_template_directory() . '/inc/post_views.php';

/**
 * Implement Like Post
 *
 * @since Springbook 1.0
 */
require_once get_template_directory() . '/inc/like_post.php';

/**
 * Implement the One Click Demo Import plugin
 *
 * @since Springbook 1.0
 */
require_once get_template_directory() . '/includes/one-click-import-data.php';


/**
 * Woocommerce support
 *
 * @since Springbook 2.0
 */

if ( ! function_exists( 'springbook_is_woocommerce_activated' ) ) {
    function springbook_is_woocommerce_activated() {
        if ( class_exists( 'woocommerce' ) ) { return true; } else { return false; }
    }
}


/**
 * Include the TGM_Plugin_Activation class.
 */
require_once get_template_directory() . '/framework/class-tgm-plugin-activation.php';

add_action('tgmpa_register', 'springbook_register_required_plugins');

/**
 * Register the required plugins for this theme.
 *
 * In this example, we register five plugins:
 * - one included with the TGMPA library
 * - two from an external source, one from an arbitrary source, one from a GitHub repository
 * - two from the .org repo, where one demonstrates the use of the `is_callable` argument
 *
 * The variables passed to the `tgmpa()` function should be:
 * - an array of plugin arrays;
 * - optionally a configuration array.
 * If you are not changing anything in the configuration array, you can remove the array and remove the
 * variable from the function call: `tgmpa( $plugins );`.
 * In that case, the TGMPA default settings will be used.
 *
 * This function is hooked into `tgmpa_register`, which is fired on the WP `init` action on priority 10.
 */
function springbook_register_required_plugins() {
    /*
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(

        // This is an example of how to include a plugin from a private repo in your theme.
        array(
            'name' => esc_html__('Redux Framework','springbook'),
             // The plugin name.
            'slug' => 'redux-framework',
             // The plugin slug (typically the folder name).
            'required' => true,
             // If false, the plugin is only 'recommended' instead of required.
            'external_url' => esc_url('https://wordpress.org/plugins/redux-framework/'),
             // If set, overrides default API URL and points to an external URL.
        ),
        array('name' => esc_html__('WPBakery Page Builder','springbook'),
             // The plugin name.
            'slug' => 'js_composer',
             // The plugin slug (typically the folder name).
            'source' => 'http://assets.cththemes.com/plugins/js_composer.zip',
             // The plugin source.
            'required' => true,
            'external_url' => esc_url('https://codecanyon.net/item/visual-composer-page-builder-for-wordpress/242431' ),
             // If set, overrides default API URL and points to an external URL.
        ), 
        array(
            'name' => esc_html__('Contact Form 7','springbook'),
             // The plugin name.
            'slug' => 'contact-form-7',
             // The plugin slug (typically the folder name).
            'required' => true,
             // If false, the plugin is only 'recommended' instead of required.
            'external_url' => esc_url('https://wordpress.org/plugins/contact-form-7/' ),
             // If set, overrides default API URL and points to an external URL.
        ), 

        array(
            'name' => esc_html__('CMB2','springbook'),
             // The plugin name.
            'slug' => 'cmb2',
             // The plugin slug (typically the folder name).
            'required' => true,
             // If false, the plugin is only 'recommended' instead of required.
            'external_url' => esc_url('https://wordpress.org/support/plugin/cmb2'),
             // If set, overrides default API URL and points to an external URL.
        ),
        array('name' => esc_html__('Springbook Add-ons','springbook'),
             // The plugin name.
            'slug' => 'springbook_add_ons',
             // The plugin slug (typically the folder name).
            'source' => 'springbook_add_ons.zip',
             // The plugin source.
            'required' => true,
             // If false, the plugin is only 'recommended' instead of required.
        ), 

        

        
        array('name' => esc_html__('Envato Market','springbook'),
             // The plugin name.
            'slug' => 'envato-market',
             // The plugin slug (typically the folder name).
            'source' => esc_url('http://envato.github.io/wp-envato-market/dist/envato-market.zip' ),
             // The plugin source.
            'required' => true,
            'external_url' => esc_url('http://envato.github.io/wp-envato-market/' ),
             // If set, overrides default API URL and points to an external URL.
            
        ),

        array('name' => esc_html__('One Click Demo Import','springbook'),
             // The plugin name.
            'slug' => 'one-click-demo-import',
             // The plugin slug (typically the folder name).
            'required' => true,
             // If false, the plugin is only 'recommended' instead of required.
            'external_url' => esc_url('https://wordpress.org/plugins/one-click-demo-import/'),
             // If set, overrides default API URL and points to an external URL.
        ),

        array('name' => esc_html__('Regenerate Thumbnails','springbook'),
             // The plugin name.
            'slug' => 'regenerate-thumbnails',
             // The plugin slug (typically the folder name).
            'required' => false,
             // If false, the plugin is only 'recommended' instead of required.
            'external_url' => esc_url('https://wordpress.org/plugins/regenerate-thumbnails/' ),
             // If set, overrides default API URL and points to an external URL.
        ),

        array('name' => esc_html__('Instagram Feed','springbook'),
             // The plugin name.
            'slug' => 'instagram-feed',
             // The plugin slug (typically the folder name).
            'required' => false,
             // If false, the plugin is only 'recommended' instead of required.
            'external_url' => esc_url('https://wordpress.org/plugins/instagram-feed/' ),
             // If set, overrides default API URL and points to an external URL.
        ),


    );

    /*
     * Array of configuration settings. Amend each line as needed.
     *
     * TGMPA will start providing localized text strings soon. If you already have translations of our standard
     * strings available, please help us make TGMPA even better by giving us access to these translations or by
     * sending in a pull-request with .po file(s) with the translations.
     *
     * Only uncomment the strings in the config array if you want to customize the strings.
     */
    $config = array(
        'id'           => 'springbook',                 // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => get_template_directory() . '/framework/plugins/',                      // Default absolute path to bundled plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.

        
    );

    tgmpa( $plugins, $config );
}

// Insert into your functions.php and have fun creating login error msgs
function guwp_error_msgs() { 
    // insert how many msgs you want as an array item. it will be shown randomly 
    $custom_error_msgs = array(
        '<strong>Você</strong> não vai passar!',
        '<strong>EI!</strong> SAIA AQUI!',
    );
    // get random array item to show
    return $custom_error_msgs[array_rand($custom_error_msgs)];;
}
add_filter( 'login_errors', 'guwp_error_msgs' );
remove_action('wp_head', 'wp_generator');





add_action('rest_api_init', 'register_rest_post_media' );
function register_rest_post_media(){
    register_rest_field( array('post'),
        'thumbnail',
        array(
            'get_callback'    => 'get_post_thumbnail',
            'update_callback' => null,
            'schema'          => null,
        )
    );
    register_rest_field( array('post'),
        'medium',
        array(
            'get_callback'    => 'get_post_medium',
            'update_callback' => null,
            'schema'          => null,
        )
    );
    register_rest_field( array('post'),
        'full',
        array(
            'get_callback'    => 'get_post_full',
            'update_callback' => null,
            'schema'          => null,
        )
    );
    register_rest_field( array('post'),
        'tags_names',
        array(
            'get_callback'    => 'get_tags_post_custom',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}
function get_post_thumbnail( $object, $field_name, $request ) {
    if( $object['featured_media'] ){
        $img = wp_get_attachment_image_src( get_post_thumbnail_id( $object->id ), 'thumbnail' );
        return $img[0];
    }
    return false;
}

function get_post_medium( $object, $field_name, $request ) {
    if( $object['featured_media'] ){
        $img = wp_get_attachment_image_src( get_post_thumbnail_id( $object->id ), 'medium' );
        return $img[0];
    }
    return false;
}

function get_post_full( $object, $field_name, $request ) {
    if( $object['featured_media'] ){
        $img = wp_get_attachment_image_src( get_post_thumbnail_id( $object->id ), 'full' );
        return $img[0];
    }
    return false;
}

function ag_filter_post_json($response, $post, $context) {
    $tags = wp_get_post_tags($post->ID);
    $response->data['tag_names'] = [];

    foreach ($tags as $tag) {
        $response->data['tag_names'][] = $tag;
    }

    return $response;
}

add_filter( 'rest_prepare_post', 'ag_filter_post_json', 10, 3 );

function get_all_post( $data, $post, $context ) {
    return [
        'id'		        => $data->data['id'],
        'slug'              => $data->data['slug'],
        'date'		        => $data->data['date'],
        'title'    	        => $data->data['title']['rendered'],
        'link'     	        => $data->data['link'],
        'featured_media'    => $data->data['featured_media'],
        'thumbnail'         => $data->data['thumbnail'],
        'medium'            => $data->data['medium'],
        'full'              => $data->data['full'],
        'content'           => $data->data['content']['rendered'],
        'tags'              => $data->data['tag_names']
    ];
}
add_filter( 'rest_prepare_post', 'get_all_post', 10, 3 );


