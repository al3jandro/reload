export interface Imagens {
  id: number;
  date?: string;
  slug?: string;
  status?: string;
  link?: string;
  title?: string;
  featured_media?: number;
  url?: string;
  position?: string;
  thumbnail?: string;
  medium?: string;
  full?: string;
}

export interface Page {
  id: number;
  slug?: string;
  title?: string;
  link?: string;
  featured_media?: number;
  thumbnail?: string;
  medium?: string;
  full?: string;
  content?: string;
}

export interface Post {
  id: number;
  slug?: string;
  title?: string;
  link?: string;
  featured_media?: number;
  thumbnail?: string;
  medium?: string;
  full?: string;
  content?: string;
}


export interface Campanha {
  id: number;
  date?: string;
  slug?: string;
  status?: string;
  link?: string;
  title?: string;
  featured_media?: number;
  url?: string;
  position?: string;
  thumbnail?: string;
  medium?: string;
  full?: string;
  home?: string;
  ordem?: number;
  texto?: string;
}

export interface Loja {
  id: number;
  date?: string;
  slug?: string;
  link?: string;
  title?: string;
  thumbnail?: string;
  medium?: string;
  full?: string;
  region: string;
  lat?: string;
  lng?: string;
  cidade?: string;
  setores?: string;
  servicos?: string;
}

export interface Tabloide {
  id: number;
  date?: string;
  slug?: string;
  link?: string;
  title?: string;
  issuu?: string;
  lat?: string;
}
