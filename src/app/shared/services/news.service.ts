import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { BehaviorSubject, Observable } from 'rxjs';
import { tap, map, retry } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { Imagens, Page, Campanha, Loja, Post, Tabloide } from './interfaces/news';

const url: string = environment.news.url;
const headers = new HttpHeaders({Authorization: `${ environment.news.key }`});
const urlBlog: string = environment.blog.url;
const headersBlog = new HttpHeaders({Authorization: `${ environment.blog.key }`});

@Injectable({
  providedIn: 'root'
})

export class NewsService {

  private loja$: BehaviorSubject<any> = new BehaviorSubject(null);
  private region$: BehaviorSubject<any> = new BehaviorSubject(null);
  private banner$: BehaviorSubject<Imagens> = new BehaviorSubject(null);

  constructor(private http: HttpClient) { }


  // Banners
  setBanners$  = (items: Imagens) => this.banner$.next(items);
  getBanners$  = (): Observable<Imagens> => this.banner$.asObservable();
  getBanners  = (): Observable<Imagens> =>  this.http.get<Imagens>(`${url}/imagens`, { params: { per_page: `100` }})
                                              .pipe(tap((data) => this.setBanners$(data)));


  // Cidade
  setRegion$  = (items: any) => this.region$.next(items);
  getRegion$  = (): Observable<any> => this.region$.asObservable();
  getRegion  = (): Observable<any> =>  this.http.get<any>(`${url}/region`, { params: { per_page: `100` }})
                                        .pipe(tap((data) => this.setRegion$(data)));

  // Loja
  setLoja$  = (items: any) => this.loja$.next(items);
  getLoja$  = (): Observable<any> => this.loja$.asObservable();
  getLoja  = (): Observable<any> =>  this.http.get<any>(`${url}/loja?_embed`, { params: { per_page: `100` }})
                                      .pipe(tap((data) => this.setLoja$(data)));


  getImage(id: number): Observable<any[]> {
    return this.http
      .get<any[]>(`${url}/media/${id}`, { headers })
      .pipe(tap(data => data));
  }

  /** Module Page */
  Page(): Observable<Page[]> {
    return this.http.get<Page[]>(`${url}/pages`)
    .pipe(tap(data => data));
  }

  LastNews(limit: number): Observable<Post[]> {
    return this.http.get<Post[]>(`${url}/posts`).pipe(retry(3), map(res => res), map((data) => data['slice'](0,limit)));
  }

  PageCollection(collection: string): Observable<Page> {
    return this.http.get<Page>(`${url}/pages${collection}`).pipe(retry(3), map(res => res));
  }

  PageSlug(slug: string): Observable<Page> {
    return this.http.get<Page>(`${url}/pages`, { params: { slug: `${slug}` }})
    .pipe(
      retry(3),
      map((res) => res[0])
      );
  }

  PageId(id: number): Observable<Page[]> {
    const sql = `${url}/pages`;
    return this.http.get<Page[]>(`${url}/pages/${id}`)
    .pipe(tap(data => data));
  }

  /** Module Post */
  Posts(p: number, page: number): Observable<any> {
    return this.http.get<any>(`${url}/posts?page=${p}&per_page=${page}`, {observe: 'response'})
    .pipe(tap(data => data));
    }

  PostSlug(slug: string): Observable<Post[]> {
    return this.http.get<Post[]>(`${url}/posts`, {
      params: { slug: `${slug}` }
    }).pipe(tap(data => data));
  }

  PostId(id: number): Observable<Post[]> {
    return this.http.get<Post[]>(`${url}/posts/${id}`)
    .pipe(tap(data => data));
  }

  PostSearch(search: string): Observable<Post[]> {
    return this.http.get<Post[]>(`${url}/posts`, { params: { search: `${search}`}, headers
    }).pipe(tap(data => data));
  }

  /** Module Imagens */
  ImagensPosition(position: string): Observable<Imagens[]> {
    return this.http.get<Imagens[]>(`${url}/imagens`, {
      params: { position }, headers
    }).pipe(
      retry(3),
      map(res => res.filter(row => row.status === 'publish' || row.position == position))
    );
  }


  /** Module Campanha */
  Campanha(slug: string): Observable<Campanha[]> {
    return this.http.get<Campanha[]>(`${url}/posto`, {
      params: { slug: `${slug}` }
    }).pipe(tap(data => data));
  }

  /** Module Loja */
  Lojas(limit: number): Observable<Loja[]> {
    return this.http
      .get<Loja[]>(`${url}/loja`, { params: { per_page: `${limit}` }, headers })
      .pipe(tap(data => data));
  }

  LojaSlug(slug: string): Observable<Loja[]> {
    return this.http
      .get<Loja[]>(`${url}/loja`, { params: { slug: `${slug}` }, headers })
      .pipe(tap(data => data));
  }

  LojaId(id: number): Observable<Loja[]> {
    return this.http
      .get<Loja[]>(`${url}/loja/${id}`)
      .pipe(tap(data => data));
  }

  LojaCodigo(id: any): Observable<Loja[]> {
    return this.http
      .get<Loja[]>(`${url}/loja`, {
        params: { meta_key: 'cod_loja', meta_value: `${id}` }, headers
      }).pipe(tap(data => data));
  }

  LojaRegion(): Observable<any[]> {
    return this.http
      .get<any[]>(`${url}/region`, { params: { per_page: `100` }, headers })
      .pipe(retry(3), map(data => data));
  }

  LojaPorRegion(id: number): Observable<Loja[]> {
    return this.http
      .get<Loja[]>(`${url}/loja`, {
        params: { region: `${id}`, per_page: `50` }, headers
      }).pipe(tap(data => data));
  }

  /** Tabloide */
  Tabloides(limit: number): Observable<Tabloide[]> {
    return this.http
      .get<Tabloide[]>(`${url}/tabloide`, { params: { per_page: `${limit}` }
      }).pipe(tap(data => data));
  }

  TabloideSlug(slug: string): Observable<Tabloide[]> {
    return this.http
      .get<Tabloide[]>(`${url}/tabloide`, { params: { slug: `${slug}` }, headers })
      .pipe(tap(data => data));
  }

  TabloideId(id: number): Observable<Tabloide[]> {
    return this.http
      .get<Tabloide[]>(`${url}/tabloide/${id}`)
      .pipe(tap(data => data));
  }



  /** Post Blog */

  Blogger = (): Observable<any> => this.http.get<any>(`${urlBlog}/posts`, {observe: 'response'});

  Blog(p: number, page: number): Observable<any> {
    return this.http
      .get<any>(`${urlBlog}/posts?page=${p}&per_page=${page}`, {observe: 'response'})
      .pipe(tap(data => data));
  }

  BlogId(id: number): Observable<any[]> {
    return this.http
      .get<any[]>(`${ urlBlog }/posts/${id}`, {headers: headersBlog})
      .pipe(tap(data => data));
  }

  BlogSlug(slug: string): Observable<any[]> {
    return this.http
      .get<any[]>(`${ urlBlog }/posts`,
      { params: { slug: `${slug}` }, headers: headersBlog})
      .pipe(tap(data => data));
  }

  BlogImage(id: number): Observable<any[]> {
    return this.http
      .get<any[]>(`${ urlBlog }/media/${id}`, {headers: headersBlog})
      .pipe(tap(data => data));
  }

  getBlogCollection(collection: string) {
    return this.http.get<any[]>(`${ urlBlog }/${collection}`, {observe: 'response'}).pipe(tap(data => data));
  }
}
