import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { NewsService } from 'src/app/shared/services/news.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lojas',
  templateUrl: './lojas.component.html',
  styleUrls: ['./lojas.component.scss']
})
export class LojasComponent implements OnInit, OnDestroy {

  p = 1;

  items: any = [];
  region: any = [];

  private lojaSubs: Subscription;
  private lojaRegionSubs: Subscription;
  private lojaXRegionSubs: Subscription;


  constructor(
    private news: NewsService,
    private router: Router
    ) { }

  ngOnInit() {
    this.lojaSubs = this.news.Lojas(100).subscribe(
      data => this.items = data,
      erro => console.log(erro)
    );
    this.getRegion();
  }

  ngOnDestroy(): void {
    if (this.lojaSubs) { this.lojaSubs.unsubscribe(); }
    if (this.lojaRegionSubs) { this.lojaRegionSubs.unsubscribe(); }
    if (this.lojaXRegionSubs) { this.lojaXRegionSubs.unsubscribe(); }
  }

  getRegion() {
    this.lojaRegionSubs = this.news.LojaRegion()
    .subscribe(data => (this.region = data));
  }

  onLoja(e: any) {
    if (e.target.value === 'All') {
      this.lojaSubs = this.news.Lojas(100)
      .subscribe(data => (this.items = data));
    } else {
      this.lojaXRegionSubs = this.news.LojaPorRegion(e.target.value)
      .subscribe(data => (this.items = data));
    }
  }

  goToLoja(e: any) {
    if (e.target.value !== 'All') {
      this.router.navigate(['lojas', e.target.value]);
    }
  }

}
