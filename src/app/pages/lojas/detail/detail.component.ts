import { Component, OnInit, OnDestroy, OnChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { NewsService } from 'src/app/shared/services/news.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SeoService } from 'src/app/shared/services/seo.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit, OnDestroy, OnChanges {

  items: any = [];
  setores: any [];
  servicos: any = [];
  imagem: any = [];
  region: any = [];
  lojas: any = [];
  slug: any = [];

  private activateSubs: Subscription;
  private lojaSlugSubs: Subscription;
  private imgSubs: Subscription;
  private lojaRegionSubs: Subscription;
  private lojaSubs: Subscription;
  private lojaXRegionSubs: Subscription;


  constructor(
    private news: NewsService,
    private activate: ActivatedRoute,
    private router: Router,
    private seo: SeoService
  ) { }

  ngOnInit() {
    this.getRegion();
    this.activateSubs = this.activate.url.subscribe(data => {
      this.slug = data[0].path;
      this.getItems(data[0].path);
    });
  }

  ngOnChanges() {
    this.getItems(this.slug);
    this.getRegion();
  }

  ngOnDestroy(): void {
    this.activateSubs.unsubscribe();
    this.lojaSlugSubs.unsubscribe();
    this.imgSubs.unsubscribe();
    this.lojaRegionSubs.unsubscribe();
    if (this.lojaXRegionSubs) { this.lojaXRegionSubs.unsubscribe(); }
    if (this.lojaSubs) { this.lojaSubs.unsubscribe(); }
  }

  getItems(slug: any) {
    this.lojaSlugSubs = this.news.LojaSlug(slug).subscribe(data => {
      this.items = data[0];
      this.setores = this.items.setores.split(',');
      this.servicos = this.items.servicos.split(',');
      this.getImage(this.items.featured_media);
    });
  }
  getImage(id: number) {
    this.imgSubs = this.news.getImage(id).subscribe(data => {
      this.imagem = data['media_details']['sizes']['full']['source_url'];
    });
  }

  getRegion() {
    this.lojaRegionSubs = this.news.LojaRegion().subscribe(data => (this.region = data));
  }

  onLoja(e: any) {
    if (e.target.value === 'All') {
      this.lojaSubs = this.news.Lojas(100).subscribe(data => (this.lojas = data));
    } else {
      this.lojaXRegionSubs = this.news.LojaPorRegion(e.target.value)
      .subscribe(data => (this.lojas = data));
    }
  }

  goToLoja(e: any) {
    if (e.target.value !== 'All') {
      this.seo.dataLayerTracking(
        {event: 'findStore', storeAction: 'Ver Loja', storeName: e.target.value }
      );
      this.ngOnInit();
      this.router.navigate(['lojas', e.target.value]);
    }
  }

}
