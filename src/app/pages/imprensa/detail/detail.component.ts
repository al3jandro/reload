import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NewsService } from '../../../shared/services/news.service';
import { Subscription, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SeoService } from '../../../shared/services/seo.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit, OnDestroy {

  subscription: Subscription;
  items: any = [];
  slug: Observable<string>;

  constructor(
    private act: ActivatedRoute,
    private news: NewsService,
    private seo: SeoService
  ) { }

  ngOnInit(): void {
    this.slug = this.act.paramMap.pipe(map(paramsMap => paramsMap.get('slug')));
    this.slug.subscribe(data => this.Post(`${data}`));
  }

  ngOnDestroy(): void {
    if (this.subscription) { this.subscription.unsubscribe(); }
  }

  Post(slug: string) {
    this.subscription = this.news.PostSlug(slug).subscribe(
      res => {
        this.items = res[0];
        this.Metatags(res[0]);
      },
      err => console.log(err)
    );
  }

  private Metatags(item) {
    this.seo.setTitle(item.title);
    this.seo.addTagName(item.title);
    this.seo.addTagDescription(item.excerpt);
    this.seo.addTagImage(item.medium);
    this.seo.addCanonical();
    this.seo.dataLayerPost('Institucional', 'Notícias', item.title);
  }
}
