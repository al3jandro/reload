import { Component, OnInit, OnDestroy } from '@angular/core';
import { NewsService } from '../../shared/services/news.service';
import { Subscription, Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-imprensa',
  templateUrl: './imprensa.component.html',
  styleUrls: ['./imprensa.component.scss']
})
export class ImprensaComponent implements OnInit, OnDestroy {

  public header: any = [];
  p: number = 1;
  ip: number = 6;
  subscription: Subscription;
  items: any;

  constructor(
    private news: NewsService
  ) { }

  ngOnInit(): void {
    this.Post(this.p);
  }

  Post(page: number) {
    this.news.Posts(page, this.ip).subscribe(
      res => {
        this.items = res.body;
        this.header = res.headers.keys().map(key => res.headers.get(key))[3];
      },
      err => console.log(err)
    );
  }

  ngOnDestroy(): void {
    if (this.subscription) { this.subscription.unsubscribe(); }
  }

  pageChanged(e: any) {
    this.p = e;
    console.log(this.p);
    this.news.Posts(this.p, this.ip).subscribe(
      res => {
        console.log(res);
        this.items = res.body;
      },
      err => console.log(err)
    );
  }
}
