import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NewsService } from '../../../shared/services/news.service';
import { UtilService } from 'src/app/shared/services/util.service';
import { ApiService } from 'src/app/shared/services/api.service';
import { Subscription, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SeoService } from '../../../shared/services/seo.service';


@Component({
  selector: 'app-campanha',
  templateUrl: './campanha.component.html',
  styleUrls: ['./campanha.component.scss']
})
export class CampanhaComponent implements OnInit, OnDestroy {

  subscription: Subscription;

  p: number = 1;
  search: string = '';
  str: number;

  view: boolean = true;
  init: boolean = false;

  loja: any = [];
  menu: any = [];
  items: any = [];
  campanha: any = [];
  departamento: any = [];

  slug: Observable<string>;

  constructor(
    private act: ActivatedRoute,
    private news: NewsService,
    private util: UtilService,
    private api: ApiService,
    private seo: SeoService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loja = this.util.StorageParse('Loja');
    this.slug = this.act.paramMap.pipe(map(paramsMap => paramsMap.get('slug')));
    this.slug.subscribe(data => {
      const url = this.router.url.split('/');
      const value = url[url.length - 1];
      if (value.split('?')[0] === 'init') { this.init = true; }
      this.Campanha(`${data}`);
    });
  }


  ngOnDestroy(): void {
    if (this.subscription) { this.subscription.unsubscribe(); }
  }

  Campanha(slug: string) {
    this.news.Campanha(slug).subscribe(
      res => {
        this.Seo(res[0].title);
        this.campanha = res[0];
        if (this.campanha.code) {
          this.MenuDepartamentoCampanha(this.loja.loja, this.campanha.code);
          this.api.OfertasLojaCampanha(this.loja.loja, this.campanha.code, 60).subscribe(
            dat => this.items = dat,
            err => console.log(err)
          );
        } else {
          this.MenuDepartamentoSlug(this.loja.loja, this.campanha.slugCampanha);
          this.api.OfertasLojaSlug(this.loja.loja, `${this.campanha.slugCampanha}`, 60).subscribe(
            dat => this.items = dat,
            err => console.log(err)
          );
        }
      },
      err => console.log(err)
    );
  }

  MenuDepartamentoSlug(loja: number, slug: string) {
    const query = `/Menus/MenuDepartamentoOfertasLojaSlug?loja=${loja}&slug=${slug}`;
    this.api.getCollection(query).subscribe(
      res => {
        this.departamento = res;
      },
      err => console.log(err)
    );
  }

  MenuDepartamentoCampanha(loja: number, campanha: number) {
    const query = `/Menus/MenuDepartamentoOfertasLojaCampanha?loja=${loja}&campanha=${campanha}`;
    this.api.getCollection(query).subscribe(
      res => this.departamento = res,
      err => console.log(err)
    );
  }

  toogle(str: number) {
    this.str = str;
    this.view = !this.view;
  }

  private Seo(departamento: string) {
    this.seo.setTitle(departamento);
    this.seo.addTagName(departamento);
    this.seo.addCanonical();
    this.seo.dataLayerProduct({
      event: 'page',
      pageType: 'Ofertas',
      productCategory: 'Campanha',
      pageTitle: departamento
    });
  }
}
