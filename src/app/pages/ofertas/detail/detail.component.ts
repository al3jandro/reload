import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../../shared/services/api.service';
import { SeoService } from '../../../shared/services/seo.service';
import { UtilService } from '../../../shared/services/util.service';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  slug: Observable<string>;
  code: any = [];
  product: any = [];
  loja: any = [];
  image: string;
  items: Observable<any[]>;

  init: boolean = false;

  constructor(
    private act: ActivatedRoute,
    private router: Router,
    private util: UtilService,
    private seo: SeoService,
    private api: ApiService
  ) { }

  ngOnInit(): void {
    this.loja = this.util.StorageParse('Loja');
    this.slug = this.act.paramMap.pipe(map(paramsMap => paramsMap.get('slug')));
    this.slug.subscribe(data => {
      const url = this.router.url.split('/');
      const value = url[url.length - 1];
      if (value.split('?')[0] === 'init') { this.init = true; }
      this.Product(this.loja.loja, `${data}`);
    });
  }

  Product(loja: number, slug: string) {
    const id = slug.split('-');
    const host = id[id.length - 1];
    // this.api.ProdutoLoja(loja, +host).subscribe(
    //   res => {
    //     this.code = res;
    //     this.product = res.produtos;
    //     this.Seo(res);
    //   },
    //   err => console.log(err)
    // );
    this.items = this.api.ProdutoLoja(loja, +host);
    this.items.subscribe(res => this.Seo(res));
  }

  br_depart(depart: any) {
    return this.router.navigate(['departamento', this.util.toSlug(this.product.dsc_departamento)]);
  }

  br_setor(depart: any, setor: any) {
    return this.router.navigate([
      'departamento',
      this.util.toSlug(this.product.dsc_departamento),
      'setor',
      this.util.toSlug(this.product.dsc_setor)
    ]);
  }

  private Seo(code: any) {
    this.seo.setTitle(`${code.produtos.dsc_produto} | Rede Condor`);
    this.seo.updateTagName(code.produtos.dsc_produto);
    this.seo.updateTagDescription(code.produtos.dsc_descricao);
    this.seo.updateTagImage(`${environment.v1.url}/Containers/produtos/download/${code.host}.jpg`);
    this.seo.addCanonical();
    this.seo.dataLayerProduct({
      pageType: 'Ofertas',
      productCategory: code.produtos.dsc_departamento,
      productSubCategory: code.produtos.dsc_setor,
      pageTitle: code.produtos.dsc_produto
    });
  }
}
