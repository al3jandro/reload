import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from 'src/app/shared/services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { UtilService } from '../../../shared/services/util.service';
import { map } from 'rxjs/operators';
import { SeoService } from '../../../shared/services/seo.service';

@Component({
  selector: 'app-departamento',
  templateUrl: './departamento.component.html',
  styleUrls: ['./departamento.component.scss']
})
export class DepartamentoComponent implements OnInit, OnDestroy {

  subscription: Subscription;

  p: number = 1;
  search: string = '';

  viewDepart: boolean = true;
  viewSector: boolean = true;
  init: boolean = false;

  loja: any = [];
  menu: any = [];
  items: any = [];
  sector: any = [];
  departamento: any = [];

  slug: Observable<string>;

  constructor(
    private act: ActivatedRoute,
    private util: UtilService,
    private api: ApiService,
    private router: Router,
    private seo: SeoService
  ) { }

  ngOnInit(): void {
    this.loja = this.util.StorageParse('Loja');
    this.slug = this.act.paramMap.pipe(map(paramsMap => paramsMap.get('slug')));
    this.slug.subscribe(data => {
      const url = this.router.url.split('/');
      const value = url[url.length - 1];
      if (value.split('?')[0] === 'init') { this.init = true; }
      this.Departamento(`${data}`);
    });
  }


  ngOnDestroy(): void {
    if (this.subscription) { this.subscription.unsubscribe(); }
  }

  Departamento(slug: string) {
    this.subscription = this.api.getMenuOfertas(`menuDepartamentoSlug?slug=${slug}`).subscribe(
      menu => {
        this.departamento = menu[0];
        this.MenuDepartamneto(this.loja.loja);
        this.MenuSector(this.loja.loja, this.departamento.codigo);
        this.api.OfertasLojaDepartamento(this.loja.loja, this.departamento.codigo).subscribe(
          res => this.items = res,
          err => console.log(err)
        );
        this.Seo(menu[0].nome);
      },
      err => console.log(err)
    );
  }

  MenuSector(loja: number, departamento: number) {
    const query = `/Menus/MenuSectorOfertasLojaDepartamento?loja=${loja}&departamento=${departamento}`;
    this.api.getCollection(query).subscribe(
      res => this.sector = res,
      err => console.log(err)
    );
  }

  MenuDepartamneto(loja: number) {
    const query = `/Menus/MenuDepartamentoOfertasLojaDepartamento?loja=${loja}`;
    this.api.getCollection(query).subscribe(
      res => this.menu = res,
      err => console.log(err)
    );
  }

  onSearch(e: any) {
    console.log(e.target.value);
  }
  toogleSector() { this.viewSector = !this.viewSector; }
  toogleDepart() { this.viewDepart = !this.viewDepart; }

  private Seo(departamento: string) {
    this.seo.setTitle(`${departamento} | Rede Condor`);
    this.seo.updateTagName(`${departamento} | Rede Condor`);
    this.seo.addCanonical();
    this.seo.dataLayerProduct({
      event: 'page',
      pageType: 'Ofertas',
      productCategory: departamento,
      pageTitle: departamento
    });
  }
}
