import { Component, OnInit, OnDestroy } from '@angular/core';
import { SeoService } from 'src/app/shared/services/seo.service';
import { NewsService } from 'src/app/shared/services/news.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit, OnDestroy {

  activate: Subscription;
  subscription: Subscription;
  items: any = [];

  slug: Observable<string>;

  constructor(
    private router: Router,
    private seo: SeoService,
    private news: NewsService,
    private act: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.slug = this.act.paramMap.pipe(map(paramsMap => paramsMap.get('id')));
    this.slug.subscribe(data => this.getPost(`${data}`));
  }

  ngOnDestroy(): void {
    if (this.activate) { this.activate.unsubscribe(); }
    if (this.subscription) { this.subscription.unsubscribe(); }
  }

  getPost(id: string) {
    this.items = this.news.PageSlug(id);
    this.items.subscribe(data => this.Seo(data));
    // this.subscription = this.news.PageSlug(id).subscribe(
    //   res => {
    //     console.log(res);
    //     this.items = res[0];
    //     this.Seo(res[0]);
    // }, err => this.router.navigate(['']));
  }

  Seo(items) {
    this.seo.setTitle(items.title);
    this.seo.updateTagName(items.title);
    // this.seo.updateTagDescription(items.content);
    this.seo.updateTagImage(items.full);
    this.seo.addCanonical();
    this.seo.dataLayerBlog({
      event: 'page',
      pageType: 'Blog',
      pageTitle: items.tile
    });
  }
}
