import { NewsService } from 'src/app/shared/services/news.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { ApiService } from 'src/app/shared/services/api.service';
import { Ofertas } from 'src/app/shared/services/interfaces/ofertas';
import { UtilService } from '../../shared/services/util.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  dia: any = [];
  semana: any = [];
  geral: any = [];
  eletro: any = [];
  campanha: any = [];
  show: boolean = true;

  ofertasdia: any = [];
  ofertasSubscription: Subscription;

  banner: any = [];
  bannerSubscription: Subscription;

  constructor(
    private util: UtilService,
    private news: NewsService,
    private api: ApiService) {}

  ngOnInit(): void {
    const loja = this.util.StorageParse('Loja');
    this.ofertasdia = this.api.getOfertas(loja.loja);
    this.ofertasSubscription = this.ofertasdia.subscribe(res => res);
    this.banner = this.news.getBanners();
    this.bannerSubscription = this.banner.subscribe(res => res);
    this.show = true;

    this.util.toData('ofertas').subscribe((res) => {
      this.dia = res[0];
      this.semana = res[1];
      this.geral = res[2];
      this.eletro = res[3];
      this.campanha = res[4];
    });
  }

  ngOnDestroy() {
    if (this.ofertasSubscription) this.ofertasSubscription.unsubscribe();
    if (this.bannerSubscription) this.bannerSubscription.unsubscribe();
  }
}
