import { Component, OnInit, OnDestroy } from '@angular/core';
import { NewsService } from '../../shared/services/news.service';
import { Subscription } from 'rxjs';
import { SeoService } from '../../shared/services/seo.service';

@Component({
  selector: 'app-tabloide',
  templateUrl: './tabloide.component.html',
  styleUrls: ['./tabloide.component.scss']
})
export class TabloideComponent implements OnInit, OnDestroy {

  show: string;
  active: any = [];
  subscription: Subscription;
  items: any = [];

  constructor(
    private news: NewsService,
    private seo: SeoService
  ) { }

  ngOnInit(): void {
    this.subscription = this.news.Tabloides(100).subscribe(data => {
      this.items = data;
      this.seo.dataLayerTracking({ event: 'clickTabloid', tabloidName: data[0].title });
      this.show = `<iframe class="embed-responsive-item" src="//e.issuu.com/embed.html#${data[0]['condor'].issuu[0]}"
                  allowfullscreen ></iframe>`;
    });
  }

  ngOnDestroy(): void { }

  Issu(issu: any, i: number) {
    this.seo.dataLayerTracking({ event: 'clickTabloid', tabloidName: issu.title.rendered });
    this.active = i;
    this.show = `<iframe class="embed-responsive-item" src="//e.issuu.com/embed.html#${issu.condor.issuu}" allowfullscreen></iframe>`;
  }
}
