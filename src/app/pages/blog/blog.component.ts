import { Component, OnInit, } from '@angular/core';
import { NewsService } from '../../shared/services/news.service';
import { Subscription, Observable } from 'rxjs';
import { NgNavigatorShareService } from 'ng-navigator-share';
import { SeoService } from '../../shared/services/seo.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {

  p: number = 1;
  ip: number = 6;
  header: any = [];
  items: any = [];
  subscription: Subscription;
  nav: boolean = true;

  constructor(
    private seo: SeoService,
    private news: NewsService,
    private navigator: NgNavigatorShareService
  ) { }

  ngOnInit(): void {
    this.Seo();
    this.Post(this.p);
    if (!this.navigator.canShare()) {
      this.nav = false;
      return;
    }
  }

  share(i: any) {
    this.navigator.share({
      title: i.title,
      text: '',
      url: `https://www.condor.com.br/blog/${i.slug}`
    }).then( (response) => {
      console.log(response);
    })
    .catch( (error) => {
      console.log(error);
    });
  }


  Post(page: number) {
    this.news.Blog(page, this.ip).subscribe(
      res => {
        this.items = res.body;
        this.header = res.headers.keys().map(key => res.headers.get(key))[3];
      },
      err => console.log(err)
    );
  }

  private Seo() {
    this.seo.setTitle('Blog');
    this.seo.addTagName('Blog');
    this.seo.addTagImage('Blog');
    this.seo.addCanonical();
    this.seo.dataLayerBlog({
      event: 'page',
      pageType: 'Blog',
    });
  }

  pageChanged(e: any) {
    this.p = e;
    this.news.Blog(this.p, this.ip).subscribe(
      res => this.items = res.body,
      err => console.log(err)
    );
  }
}

