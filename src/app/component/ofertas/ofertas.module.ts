import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OfertasComponent } from './ofertas.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { RouterModule } from '@angular/router';
import { HelperModule } from '../helper/helper.module';
import { CarouselModule } from 'ngx-owl-carousel-o';

@NgModule({
  declarations: [OfertasComponent],
  exports: [OfertasComponent],
  imports: [
    HelperModule,
    RouterModule,
    CommonModule,
    CarouselModule,
    MDBBootstrapModule
  ]
})

export class OfertasModule { }
