import { Component, OnInit, Input } from '@angular/core';

import { OwlOptions } from 'ngx-owl-carousel-o';

import { Subscription, Observable, timer } from 'rxjs';
import { map, debounceTime, tap } from 'rxjs/operators';

import { ApiService } from '../../shared/services/api.service';
import { Ofertas } from 'src/app/shared/services/interfaces/ofertas';
import { UtilService, LojaStorage } from '../../shared/services/util.service';


@Component({
  selector: 'app-ofertas',
  templateUrl: './ofertas.component.html',
  styleUrls: ['./ofertas.component.scss'],
})
export class OfertasComponent implements OnInit {
  @Input() code: any;
  @Input() sector?: number;
  @Input() type?: any;
  @Input() title: string;
  @Input() icon: string;
  @Input() link: string;
  items: Observable<Ofertas>;
  test: Observable<Ofertas>;
  loja: LojaStorage;
  subscription: Subscription;

  ofertas: any = [];
  isLoading: boolean = true;

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    dots: false,
    navSpeed: 500,
    margin: 10,
    autoplay: true,
    nav: true,
    navText: [
      '<i class="fas fa-chevron-left"></i>',
      '<i class=" fas fa-chevron-right"></i>',
    ],
    responsive: {
      0: { items: 2, nav: true },
      480: { items: 3, nav: true },
      768: { items: 3, nav: true },
      1024: { items: 4, nav: true },
    },
  };

  constructor(
    private api: ApiService,
    private util: UtilService
  ) {}

  ngOnInit(): void {
    timer(300).subscribe(() => {
      switch (this.type) {
        case 'slug': {
          this.ofertasSlug(this.code);
          break;
        }
        case 'campanha': {
          this.ofertasCampanha(this.code);
          break;
        }
        default: {
          this.ofertasDepartamento(this.code);
          break;
        }
      }
    })
  }

  ofertasSlug = (code: string) => this.items = this.api.getOfertas$()
    .pipe(
      debounceTime(800),
      map((res) => {
        if(res) {
          const result = res['filter']((row => row.slug === code))
          return result.slice(0,15)
        }
      })
    );

  ofertasCampanha = (code: number) => this.items = this.api.getOfertas$()
  .pipe(
    debounceTime(800),
    map((res) => {
      if(res) {
        const result = res['filter']((row => row.campanha === code))
        return result.slice(0,15)
      }
    })
  );


  ofertasDepartamento = (code: number) => this.items = this.api.getOfertas$()
  .pipe(
    debounceTime(800),
    map((res) => {
      if(res) {
        const result = res['filter']((row => row.departamento === code))
        return result.slice(0,15)
      }
    })
  );

  // Sector(code: any, sector: number) {
  //   this.items = this.api.OfertasLojaDepartamentoSetor(this.loja['loja'], +code, sector, 10)
  //   .pipe(
  //     map((res) => {
  //       const arr = [];
  //       res.forEach(el => {
  //         if(el.produtos){
  //           arr.push(el);
  //         }
  //       })
  //       return arr;
  //     })
  //   );
  // }
}
