import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ApiService } from '../../shared/services/api.service';
import { UtilService } from '../../shared/services/util.service';
import { MenuDepartamento } from '../../shared/services/interfaces/menu';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})

export class MenuComponent implements OnInit {

  json: Observable<any>;
  campanha: Observable<any>;
  items: Observable<MenuDepartamento>;

  constructor(
    private api: ApiService,
    private util: UtilService
  ) { }

  ngOnInit(): void {
    this.items = this.api.getMenuOfertas('menuDepartamento');
    this.json = this.util.getMenu().pipe(map(res => res.center));
    this.campanha = this.util.getMenu().pipe(map(res => res.campanha));
  }

}
