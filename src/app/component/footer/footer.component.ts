import { Component, OnInit } from '@angular/core';
import { UtilService } from '../../shared/services/util.service';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  items: any = [];

  constructor(private util: UtilService) {}

  ngOnInit() {
    this.util.toData('footer').subscribe(res => this.items = res);
  }

}
