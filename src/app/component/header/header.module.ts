import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { RouterModule } from '@angular/router';
import { HelperModule } from '../helper/helper.module';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';

@NgModule({
  declarations: [HeaderComponent],
  bootstrap: [HeaderComponent],
  imports: [
    CommonModule,
    RouterModule,
    HelperModule,
    AutocompleteLibModule,
    MDBBootstrapModule,
    NgxSkeletonLoaderModule
  ]
})
export class HeaderModule { }
