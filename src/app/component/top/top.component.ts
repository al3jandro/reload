import { Observable } from 'rxjs';

import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { UtilService } from '../../shared/services/util.service';
import { SelectorComponent } from '../helper/selector/selector.component';
import { ApiService } from '../../shared/services/api.service';
import { map } from 'rxjs/operators';

@Component({
    selector: 'app-top',
    templateUrl: './top.component.html',
    styleUrls: ['./top.component.scss']
})
export class TopComponent implements OnInit {

    @Input() entrada: boolean = false;
    @Output() salida = new EventEmitter<boolean>();

    loja: any = [];
    json: Observable<any>;
    items: Observable<any>;
    ofertas: Observable<any>;

    active1: boolean = true;
    active2: boolean = true;
    toogle1: boolean = false;

    constructor(
        private api: ApiService,
        private util: UtilService) { }

    ngOnInit(): void {
      this.json = this.util.getTop$();
      this.items = this.util.getMenu();
      this.ofertas = this.api.getMenuOfertas('menuDepartamento');
      this.loja = this.util.StorageParse('Loja');
    }

    active = () => this.toogle1 = !this.toogle1;
    submenu1 = () => this.active1 = !this.active1;
    submenu2 = () => this.active2 = !this.active2;

    toogle = () => {
        this.entrada = !this.entrada;
        this.salida.emit(this.entrada);
    }
}
