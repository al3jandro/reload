import { Component, OnInit, Input } from '@angular/core';
import { SeoService } from '../../../shared/services/seo.service';
import { Router } from '@angular/router';
import { UtilService } from '../../../shared/services/util.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'helper-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
})
export class ProductComponent implements OnInit {

  @Input() code: any = [];
  product: any = [];
  imagem: any = [];
  loja: any = [];

  constructor(
    private util: UtilService,
    private router: Router,
    private seo: SeoService
  ) { }

  ngOnInit() {
    setTimeout(() => {
      this.product = this.code.produtos;
      this.loja = this.util.StorageParse('Loja');
      this.imagem = `${environment.v1.url}/Containers/produtos/download`;
    }, 500);
  }

  goToProduct() {
    this.seo.dataLayerTracking({
      event: 'productInteraction',
      productAction: 'Visualizar Produto',
      productName: this.product.dsc_produto
    });
    this.router.navigate([
      'produto',
      this.util.toSlug(this.product.dsc_departamento),
      this.util.toSlug(this.product.dsc_setor),
      this.code.produtos.slug
    ]);
  }
}
