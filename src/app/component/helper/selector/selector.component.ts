import { Component, OnInit, Output, Input, ViewChild, OnDestroy } from '@angular/core';
import { NewsService } from '../../../shared/services/news.service';
import { Subscription, Observable, timer } from 'rxjs';
import { SeoService } from '../../../shared/services/seo.service';
import { UtilService } from '../../../shared/services/util.service';
import { Loja } from 'src/app/shared/services/interfaces/news';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/shared/services/api.service';

const objeto = { loja: 21, slug: 'hiper-condor-nilo-pecanha', nome: 'Hiper Condor Nilo Peçanha' };

@Component({
  selector: 'helper-selector',
  templateUrl: './selector.component.html',
  styleUrls: ['./selector.component.scss']
})
export class SelectorComponent implements OnInit, OnDestroy {

  @Output() loja: any;
  @Input() new: any;
  @Input() type?: string = 'simple';
  @Input() load?: boolean = false;
  @ViewChild('frame', { static: true }) public frame: any;
  region: Observable<any[]>;
  lojas: Observable<Loja[]>;
  miObjeto: any = [];
  condor: number;

  private lojaSubscription: Subscription;

  constructor(
    private news: NewsService,
    private router: Router,
    private seo: SeoService,
    private api: ApiService,
    private util: UtilService
  ) {
  }

  ngOnInit() {
    timer(800).subscribe(() => {
      this.active();
      if (this.load === true) this.frame.show();
      this.region = this.news.getRegion$();
    })
  }

  ngOnDestroy(): void {
    if (this.lojaSubscription) {this.lojaSubscription.unsubscribe(); }
  }


  getLojas =(e: any) => this.lojas = this.news.LojaPorRegion(e.target.value);
  active = () => { if (this.util.StorageParse('Loja')) this.miObjeto = this.getLocalStorageLoja(); }


  open() {
    this.frame.show();
    this.seo.dataLayerTracking({
      event: 'findStore',
      storeAction: 'Clique Header',
      storeName: 'Nome da Loja'
    });
  }

  close() {
    this.frame.hide();
  }

  setLocalStorageLoja() {
    this.util.StorageAddKey('Loja', objeto);
  }

  getLocalStorageLoja() {
    return this.util.StorageParse('Loja');
  }


  selectCondor() {
    this.lojaSubscription = this.news.LojaId(this.condor).subscribe(data => {
      const cod = data['condor']['cod_loja'][0];
      this.miObjeto = {
        loja: cod,
        slug: data['slug'],
        nome: data['title'].rendered
      };
      // this.seo.dataLayerTracking({
      //   event: 'findStore',
      //   storeAction: 'Selecionar Cidade',
      //   storeCity: data['cidade'],
      //   storeName: data['title'].rendered
      // });
      this.util.StorageRemoveKey('Loja');
      this.util.StorageAddKey('Loja', this.miObjeto);
    });
    this.frame.hide();
    setTimeout(() => {
      this.init();
    }, 300);
  }

  init() {
    const www = 'https://www.condor.com.br';
    const url = this.router.url.split('/');
    const value = url[url.length - 1 ];
    if (value.split('?').shift() === 'init')
      if (url[url.length - 3 ] === 'departamento') window.location.href = `${www}/${url[1]}/${url[2]}`;
      else if (url[url.length - 3 ] === 'setor') window.location.href = `${www}/${url[1]}/${url[2]}/${url[3]}/${url[4]}`;
      else window.location.href = `${www}/produto/${url[2]}/${url[3]}/${url[4]}`;
    else window.location.reload();
  }
}
