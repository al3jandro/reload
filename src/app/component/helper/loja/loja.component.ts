import { Component, OnInit, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { NewsService } from 'src/app/shared/services/news.service';

@Component({
  selector: 'helper-loja',
  templateUrl: './loja.component.html',
  styleUrls: ['./loja.component.scss']
})
export class LojaComponent implements OnInit {

  @Input() loja: any = [];
  imagem: any = [];

  private imageSubs: Subscription;

  constructor(private news: NewsService) { }

  ngOnInit() {
    this.imageSubs = this.news.getImage(this.loja.featured_media).subscribe(data => {
      this.imagem = data['media_details']['sizes']['medium']['source_url'];
    });
  }

  ngOnDestroy(): void {
    if (this.imageSubs) { this.imageSubs.unsubscribe(); }
  }

}
