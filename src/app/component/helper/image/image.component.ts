import { Component, Input } from '@angular/core';

@Component({
  selector: 'helper-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent {
  @Input() code: any = [];
  @Input() idImage?: string = '';
  @Input() size?: string;
  imagem: any = [];
  default = `https://via.placeholder.com/${this.size}?text=Rede+Condor`;
}
