import {
  BrowserModule,
  BrowserTransferStateModule,
  HAMMER_GESTURE_CONFIG,
  HammerGestureConfig
} from '@angular/platform-browser';
import { NgModule, Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AppComponent } from './app.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APP_ROUTE } from './app.routes';
import { LoadableModule, matcher } from 'ngx-loadable';
import localePt from '@angular/common/locales/pt';
import { registerLocaleData } from '@angular/common';
import { HelperModule } from './component/helper/helper.module';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { EXTERNAL_ROUTES } from './external.routes';
import { RedirectGuard } from './shared/services/redirect.guard';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { NgtUniversalModule } from '@ng-toolkit/universal';
import {NgcCookieConsentModule, NgcCookieConsentConfig} from 'ngx-cookieconsent';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
registerLocaleData(localePt);

declare var Hammer: any;

@Injectable()
export class MyHammerConfig extends HammerGestureConfig {
  // tslint:disable-next-line: no-angle-bracket-type-assertion
  overrides = <any> {
    pan: { direction: Hammer.DIRECTION_All },
    swipe: { direction: Hammer.DIRECTION_VERTICAL },
  } as any;
  buildHammer(element: HTMLElement) {
    const mc = new Hammer(element, {
      touchAction: 'auto',
      inputClass: Hammer.SUPPORT_POINTER_EVENTS
        ? Hammer.PointerEventInput
        : Hammer.TouchInput,
      recognizers: [[Hammer.Swipe, { direction: Hammer.DIRECTION_HORIZONTAL }]],
    });
    return mc;
  }
}

const cookieConfig:NgcCookieConsentConfig = {
  cookie: { domain: 'localhost' },
  position: "bottom",
  theme: "edgeless",
  palette: {
    popup:  { background: "#000000", text: "#ffffff" },
    button: { background: "#f1d600", text: "#000000" }
  },
  layout: 'my-custom-layout',
  layouts: {
    "my-custom-layout": '{{messagelink}}{{compliance}}'
  },
  elements:{
    messagelink: `
    <span id="cookieconsent:desc" class="cc-message">{{message}} 
      <a aria-label="learn more about cookies" tabindex="0" class="cc-link cc-politica" href="#">Políticas de privacidade</a> e as 
      <a aria-label="learn more about our privacy policy" tabindex="1" class="cc-link cc-regras" href="#">Termo de uso</a>. 
    </span>
    `,
  },
  type: 'opt-in'
};

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserTransferStateModule,
    MDBBootstrapModule.forRoot(),
    BrowserAnimationsModule,
    NgxSkeletonLoaderModule,
    HttpClientModule,
    NgtUniversalModule,
    APP_ROUTE,
    EXTERNAL_ROUTES,
    HelperModule,
    NgcCookieConsentModule.forRoot(cookieConfig),
    TranslateModule.forRoot({ loader: { provide: TranslateLoader, useFactory: HttpLoaderFactory, deps: [HttpClient] } }),
    LoadableModule.forRoot({
      moduleConfigs: [
        {
          name: 'header',
          loadChildren: () =>
            import('src/app/component/header/header.module').then(
              (m) => m.HeaderModule
            ),
          matcher,
        },
        {
          name: 'menu',
          loadChildren: () =>
            import('src/app/component/menu/menu.module').then(
              (m) => m.MenuModule
            ),
          matcher,
        },
        {
          name: 'top',
          loadChildren: () =>
            import('src/app/component/top/top.module').then((m) => m.TopModule),
          matcher,
        },
        {
          name: 'footer',
          loadChildren: () =>
            import('src/app/component/footer/footer.module').then(
              (m) => m.FooterModule
            ),
          matcher,
        },
        {
          name: 'mobile',
          loadChildren: () =>
            import('src/app/component/mobile/mobile.module').then(
              (m) => m.MobileModule
            ),
          matcher,
        },
      ],
    }),
    ServiceWorkerModule.register('OneSignalSDKWorker.js', {
      enabled: environment.production,
      registrationStrategy: 'registerImmediately'
    }),
  ],
  providers: [
    { provide: HAMMER_GESTURE_CONFIG, useClass: MyHammerConfig },
    { provide: 'googleTagManagerId', useValue: 'GTM-T7FLP2C' },
    RedirectGuard,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
